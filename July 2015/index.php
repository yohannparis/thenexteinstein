<?php
	error_reporting(E_ALL); // Error Reporting
	ob_start('ob_gzhandler'); // Compress output and turn on buffer
	header('content-type:text/html; charset=utf-8'); // Set type and charset

?><!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
	<style type="text/css"><?php include 'waiting.css'; ?></style>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>The Next Einstein</title>
	<link rel="icon" type="image/png" href="/favicon.png">
</head>
<body class="home">

	<header>
		<img src="/model/waiting-page/TNE_Logo.png" alt="The Next Einstein" width="226" height="52">
		<a href="/">Home</a>
		<a href="/winners.php">Winners</a>
	</header>

	<h1>Einstein changed the world with one idea.<br>Now it's your turn.</h1>

	<img class="einstein" src="/model/waiting-page/TNE_ComingSoon_Einstein.png" alt="Einstein" width="495" height="597">

	<p>The Next Einstein is a competition with an audacious goal: to discover and celebrate ideas that could change the world.</p>

	<img class="handwriting" src="/model/waiting-page/checksoon.png" alt="We're brainstorming. Check back soon!" width="402" height="103">

	<form action="" method="post">
		<p>For 2015 contest updates please enter your email</p>
		<input id="newsletterEmail" type="email" name="email" placeholder="EMAIL"><input id="newsletterSubscribe" type="submit" value="SUBMIT">

		<script async src="/view/js/ajax.js"></script>
		<script async src="/view/js/newsletter.js"></script>
	</form>

	<a class="social" href="https://www.facebook.com/thenexteinstein">
		<img src="/model/waiting-page/Facebook.png" alt="Facebook" width="69" height="69">
	</a>
	<a class="social twitter" href="https://twitter.com/thenxt_einstein">
		<img src="/model/waiting-page/twitter.png" alt="Twitter" width="69" height="69">
	</a>

	<script async src="//cdn.polyfill.io/v1/polyfill.js"></script>
	<!--[if lt IE 9]>
		<script async src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script async src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
	<![endif]-->
</body>
</html>
<?php ob_end_flush(); // Send the Output Buffering ?>
