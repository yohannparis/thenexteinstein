// ----- Newsletter

	// Newsletter Handler
	function newsletterHandler(button){

		try{
			if(httpRequest.readyState < 4){
				httpRequest.button.classList.add('ajax-spinner');

			} else if(httpRequest.readyState === 4){

				if(httpRequest.status === 200){
					var response = JSON.parse(httpRequest.responseText);

					if(response.sent){
						httpRequest.button.classList.remove('ajax-spinner');
						httpRequest.button.classList.add('ajax-success');
						httpRequest.button.setAttribute('disabled', 'disabled');
						httpRequest.button.setAttribute('value', 'THANK YOU');

					} else {
						httpRequest.button.classList.remove('ajax-spinner');
						httpRequest.button.classList.add('ajax-error');
					}

				} else {
					httpRequest.button.classList.add('ajax-error');
					console.log('Error wrong response.');
				}
			}
		} catch(e){
			httpRequest.button.classList.add('ajax-error');
			console.log('Error Server down.');
		}
	}

	// Send email to mailing list on mailchimp
	var newsletterEmail = document.getElementById('newsletterEmail');
	var newsletterButton = document.getElementById('newsletterSubscribe');
	var newsletterUrl = '/controller/subscribe-to-newsletter.php';

	newsletterButton.addEventListener('click', function (event){
		event.preventDefault();
		var newsletterData = 'email='+newsletterEmail.value;
		makeRequest(newsletterButton, newsletterHandler, newsletterUrl, newsletterData);
	});
