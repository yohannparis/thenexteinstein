<?php

/* ==== Menu of submitting an entry to NAA 2013 ==== */

function menuWorkflow($DBlink, $entryCode, $userID, $step)
{
	// Get the current step process of the entry
	$stepProcess = stepProcessEntryUser($DBlink, $entryCode, $userID);

	$title[1] = 'Entrant(s) Information';
	$title[2] = 'Entry';
	$title[3] = 'Artwork Upload';
	$title[4] = 'Review';
	$title[5] = 'Payment';
	$title[6] = 'Confirmation';

	$menu = '';
	for ($i=1;$i<7;$i++){
		$menu .= '<a ';

		// Check if this is the current page
		$menu .= ($step == $i ? 'class="active" ' : '' );

		// Check if this step has been processed
		$menu .= ($i <= $stepProcess ? 'class="done" ' : '' );

		// You cannot go back after the review
		$menu .= ($stepProcess < 5 && $stepProcess >= $step && $i <= 4 ? 'href="/submit/step'.$i.'"' : '' );

		$menu .= '>'.$title[$i].'</a>';
	}

	return $menu;
}


/* ==== Workflow of submitting an entry to NAA 2013 ==== */

/*
	{0} Step 0 - From the Dashboard
	-----------------------------------------------------------------
	The user select a category and click [NEXT].
		- Create a new Entry into the DBB with an Unique Entry Code.
		- Set the <stepProcess> of the entry to 1.

	OR: click [continue] on one of the entry previously started and listed on the dashboard.
*/

function genUniqueId($len=5)
{
	$hex  = md5(SALT.uniqid("", true));
	$pack = pack('H*', $hex);
	$tmp  = base64_encode($pack);
	$uid  = preg_replace("#(*UTF8)[^A-Za-z0-9]#", "", $tmp);

	return substr($uid, 0, $len);
}

function getEntryCode($DBlink)
{
	do {
		$uid = genUniqueId();
		$result = mysqli_query($DBlink, "SELECT `entryCode` FROM `entries` WHERE `entryCode` = '$uid';");

	} while (mysqli_num_rows($result) > 0);

	return $uid;
}

function step0($DBlink)
{
	// Create the entryCode
	$entryCode = getEntryCode($DBlink);
	$userID = $_SESSION['id'];

	// Create a new entry
	$request = "INSERT INTO entries (entries.entryCode, entries.userID, entries.stepProcess, entries.date) VALUES ('$entryCode', $userID, 1, SYSDATE());";

	if (mysqli_query($DBlink, $request)){
		return true;
	} else return false;
}

/*
	{1} Step 1 - Entrant
	-----------------------------------------------------------------
	The user enter all the information about the two entrants.

	SUBMIT:
		UPDATE the information into the entry.
		Set the <stepProcess> of the entry to 2.
*/

function step1($DBlink, $entryCode)
{
	// - Update the Step Process to 2
			$update  = "entries.stepProcess = ".   cleanOutput($_POST['stepProcess'])  .",";

	// - Entrants 1
			$update .= "entries.E1_agency = '".    cleanOutput($_POST['E1_agency'])    ."',";
			$update .= "entries.E1_role = '".    	 cleanOutput($_POST['E1_role'])    	 ."',";
			$update .= "entries.E1_firstName = '". cleanOutput($_POST['E1_firstName']) ."',";
			$update .= "entries.E1_lastName = '".  cleanOutput($_POST['E1_lastName'])  ."',";
			$update .= "entries.E1_address1 = '".  cleanOutput($_POST['E1_address1'])  ."',";
			$update .= "entries.E1_city = '".      cleanOutput($_POST['E1_city'])      ."',";
			$update .= "entries.E1_province = '".  cleanOutput($_POST['E1_province'])  ."',";
			$update .= "entries.E1_postalCode = '".cleanOutput($_POST['E1_postalCode'])."',";
			$update .= "entries.E1_email = '".     cleanOutput($_POST['E1_email'])     ."',";
			$update .= "entries.E1_phone = '".     cleanOutput($_POST['E1_phone'])     ."',";

		if (isset($_POST['E1_address2']))
		{
			$update .= "entries.E1_address2 = '".  cleanOutput($_POST['E1_address2'])  ."',";
		}

		if (isset($_POST['E1_fax']))
		{
			$update .= "entries.E1_fax = '".       cleanOutput($_POST['E1_fax'])       ."',";
		}

		if (isset($_POST['E1_website']))
		{
			$update .= "entries.E1_website = '". cleanOutput($_POST['E1_website']) ."',";
		}

	// - Entrants 2
	if (isset($_POST['E2']))
	{
			$update .= "entries.E2 = 1,";
			$update .= "entries.E2_agency = '".    cleanOutput($_POST['E2_agency'])    ."',";
			$update .= "entries.E2_role = '".    	 cleanOutput($_POST['E2_role'])    	 ."',";
			$update .= "entries.E2_firstName = '". cleanOutput($_POST['E2_firstName']) ."',";
			$update .= "entries.E2_lastName = '".  cleanOutput($_POST['E2_lastName'])  ."',";
			$update .= "entries.E2_address1 = '".  cleanOutput($_POST['E2_address1'])  ."',";
			$update .= "entries.E2_city = '".      cleanOutput($_POST['E2_city'])      ."',";
			$update .= "entries.E2_province = '".  cleanOutput($_POST['E2_province'])  ."',";
			$update .= "entries.E2_postalCode = '".cleanOutput($_POST['E2_postalCode'])."',";
			$update .= "entries.E2_email = '".     cleanOutput($_POST['E2_email'])     ."',";
			$update .= "entries.E2_phone = '".     cleanOutput($_POST['E2_phone'])     ."',";

		if (isset($_POST['E2_address2']))
		{
			$update .= "entries.E2_address2 = '".  cleanOutput($_POST['E2_address2'])  ."',";
		}

		if (isset($_POST['E2_fax']))
		{
			$update .= "entries.E2_fax = '".       cleanOutput($_POST['E2_fax'])       ."',";
		}

		if (isset($_POST['E2_website']))
		{
			$update .= "entries.E2_website = '". cleanOutput($_POST['E2_website']) ."',";
		}
	}

	// They are no Entrants 2 and we delete everything about him
	else
	{
		$update .= "entries.E2            = 0,
					entries.E2_agency     = '',
					entries.E2_firstName  = '',
					entries.E2_lastName   = '',
					entries.E2_address1   = '',
					entries.E2_address2   = '',
					entries.E2_city       = '',
					entries.E2_province   = '',
					entries.E2_postalCode = '',
					entries.E2_email      = '',
					entries.E2_phone      = '',
					entries.E2_fax        = '',
					entries.E2_role       = '',
					entries.E2_website    = '',";
	}

	// UPDATE the information into the entry
	$request = "UPDATE entries
				   SET ".substr($update, 0, -1)."
				 WHERE entryCode = '".$entryCode."';";

	if (mysqli_query($DBlink, $request))
	{
		$_SESSION['stepProcess']  = cleanOutput($_POST['stepProcess']);
		return true;
	}
	else return false;
}

/*
   {2} Step 2 - Entry
	-----------------------------------------------------------------
	The user enter all the information about the entry and credits.

	SUBMIT:
		UPDATE the information into the entry.
		Set the <stepProcess> of the entry to 3.
*/

function step2($DBlink, $entryCode)
{
	// - Update the Step Process
			$update  = "entries.stepProcess = ". cleanOutput($_POST['stepProcess']).",";

	// - Category
			$update .= "entries.category = '".cleanOutput($_POST['category'])."',";

	// - Entry Information
			$update .= "entries.title = '".cleanOutput($_POST['title'])."',";
			$update .= "entries.description = '".mysqli_real_escape_string($DBlink, $_POST['description'])."',";

	// UPDATE the information into the entry
	$request = "UPDATE entries
				   SET ".substr($update, 0, -1)."
				 WHERE entryCode = '".$entryCode."';";

	if (mysqli_query($DBlink, $request))
	{
		$_SESSION['stepProcess']  = cleanOutput($_POST['stepProcess']);
		return true;
	}
	else return false;
}

/*
	{3} Step 3 - Artwork
	-----------------------------------------------------------------
	The user uploads the file into the S3 bucket naa2013 at each success we add a hidden field to the step2 form.

	SUBMIT:
		INSERT the list of uploaded file on the <Upload> table with the <S3link> with the <entryCode>
		Set the <stepProcess> of the entry to 4.
*/

function step3($DBlink, $entryCode)
{

	$i = 0; // Files counter
	while (isset($_POST['nameFile'.$i]) and isset($_POST['keyFile'.$i]))
	{
		// Create a new entry
		$request = "INSERT INTO uploads (uploads.entryCode, uploads.userID, uploads.name, uploads.key, uploads.date)
						  VALUES ('$entryCode', ".$_SESSION['id'].", '".$_POST['nameFile'.$i]."', '".$_POST['keyFile'.$i]."', SYSDATE());";

		// Execute the request
		if (mysqli_query($DBlink, $request)){
			$i++;
		}
		else break;
	}


	// UPDATE the Step Process of the entry
	$request = "UPDATE entries
				   SET entries.stepProcess = 4
				 WHERE entryCode = '".$entryCode."'
				   AND userID = ".$_SESSION['id'].";";

	if (mysqli_query($DBlink, $request))
	{
		$_SESSION['stepProcess'] = 4;
		return true;
	}
	else return false;
}

/*
	{4} Step 4 - Review
	-----------------------------------------------------------------
	The user review the information. He can edit them by going back to the previous steps.

	SUBMIT:
		- Set the <stepProcess> of the entry to 5.
		  At this point you cannot change anything on the entry.
*/

function step4($DBlink, $entryCode)
{
	// UPDATE the information into the entry
	$request = "UPDATE entries
				   SET entries.stepProcess = 5
				 WHERE entryCode = '".$entryCode."';";

	if (mysqli_query($DBlink, $request))
	{
		$_SESSION['stepProcess'] = 5;
		return true;
	}
	else return false;
}

/*
	{5} Step 5 - Payment
	-----------------------------------------------------------------
	The user will pay the fees b y credit card using Stripe.

	In case of success of the payment, we send a receipt email.
	Else the user cannot go forward.

	SUBMIT:
		- Set the <stepProcess> of the entry to 6.
		At this point the entry is finalise and nothing can be change or done.
*/

function step5free($DBlink, $entryCode, $entryInfo)
{
	$email = emailConfirmation($_SESSION['email'], $entryInfo, $amount, $charge);

	// UPDATE the information into the entry
	$request = "UPDATE entries
				   SET entries.stepProcess = 6
				 WHERE entryCode = '".$entryCode."';";

	if (mysqli_query($DBlink, $request))
	{
		$_SESSION['stepProcess'] = 6;
		return true;
	}
	else return false;
}

function step5($DBlink, $entryCode, $token, $amount, $entryInfo)
{
	// set your secret key: remember to change this to your live secret key in production
	// see your keys here https://manage.stripe.com/account
	Stripe::setApiKey("sk_live_SPRZNEpuEpU3CCUobJrkca20");
	//Stripe::setApiKey("sk_test_7tQ6amoQ3qJt5GxZWgSgCcDd");

	// create the charge on Stripe's servers - this will charge the user's card
	try {
		$charge = Stripe_Charge::create(array(
			"amount" => ($amount*1.13)*100, // amount in cents, again
			"currency" => "cad",
			"card" => $token,
			"description" => $_SESSION['email'].' - '.$entryCode.' - '.$entryInfo['category'])
		);

		if($charge['paid'])
		{
			$email = emailConfirmation($_SESSION['email'], $entryInfo, $amount, $charge);

			// UPDATE the information into the entry
			$request = "UPDATE entries
						   SET entries.stepProcess = 6
						 WHERE entryCode = '".$entryCode."';";

			if (mysqli_query($DBlink, $request))
			{
				$_SESSION['stepProcess'] = 6;
				return true;
			}
			else return false;
		}
		else return $charge['failure_message'];
	}
	catch(Stripe_CardError $e)
	{
		// Since it's a decline, Stripe_CardError will be caught
		$body = $e->getJsonBody();
		$error  = $body['error'];

		if($error['code'] === 'card_declined')
		{
			return 'The card was declined.';
		}
		else if($error['code'] === 'expired_card')
		{
			return 'The card has expired.';
		}
		else
		{
			return 'The card information are incorrect.';
		}
	}
	catch (Stripe_InvalidRequestError $e)
	{
		// Invalid parameters were supplied to Stripe's API
		mail('developer@metricksystem.com', 'NAC - Stripe Error - Invalid API Request', 'Invalid parameters were supplied to Stripe\'s API');
		return 'An error occurred while processing the card.';
	}
	catch (Stripe_AuthenticationError $e)
	{
		// Authentication with Stripe's API failed (maybe you changed API keys recently)
		mail('developer@metricksystem.com', 'NAC - Stripe Error - API authentification', 'Authentication with Stripe\'s API failed (maybe you changed API keys recently)');
		return 'An error occurred while processing the card.';
	}
	catch (Stripe_ApiConnectionError $e)
	{
		// Network communication with Stripe failed
		mail('developer@metricksystem.com', 'NAC - Stripe Error - API connection', ' Network communication with Stripe failed');
		return 'An error occurred while processing the card.';
	}
	catch (Stripe_Error $e)
	{
		// Display a very generic error to the user, and maybe send yourself an email
		mail('developer@metricksystem.com', 'NAC - Stripe Error - Generic', 'Display a very generic error ');
		return 'An error occurred while processing the card.';
	}
	catch (Exception $e)
	{
		// Something else happened, completely unrelated to Stripe
		mail('developer@metricksystem.com', 'NAC - Stripe Error - Unrelated', 'Something else happened, completely unrelated to Stripe');
		return 'An error occurred while processing the card.';
	}
}
