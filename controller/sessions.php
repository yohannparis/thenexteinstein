<?php

// ---- Sessions
ini_set('session.use_only_cookies', true);
session_start();


// ---- Login flag
$logged = false;
if (isset($_SESSION['isLogin']) and $_SESSION['isLogin']){$logged = true;}


// ---- Session Security
// The Session Id is regenerate every 30 sec to avoid Session theft attack
if (!isset($_SESSION['generated']) || $_SESSION['generated'] < (time()-30)){
	session_regenerate_id();
	$_SESSION['generated'] = time();
}


// ---- Clean the session but the user information
function cleanSession()
{
	// Keep temporaly the user info
	$tmpID    = $_SESSION['id'];
	$tmpEmail = $_SESSION['email'];

	// Empty the Session
	$_SESSION = array();

	// Set back the user information
	$_SESSION['isLogin'] = true;
	$_SESSION['id']      = $tmpID;
	$_SESSION['email']   = $tmpEmail;
}
