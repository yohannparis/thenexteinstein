<?php

// ---- Email Signature
function emailSignature(){
	return '
		<p style="font-size:11px; color:#2E3192; margin-top:40px;">
			<strong>ELLIE METRICK</strong></br/>
			Marketing and Communication Manager
			<br/>—<br/>
			<strong>NATIONAL ADVERTISING CHALLENGE</strong></br/>
			100 Miranda Avenue, 2nd Floor<br/>
			Toronto, ON, M6B 3W7<br/>
			Canada<br/>
			<br/>
			+1 416 203-0753 x54</br/>
			<a href="https://nationaladvertisingchallenge.com">nationaladvertisingchallenge.com</a>
		</p>
	';
}



// ================================================================================= //
// Email Gala Confirmation
// Arguments:
//  - email: email address of the payer
//  - the object content with all the information from Stripe
// ================================================================================= //

function emailGalaConfirmation($email, $charge, $ticketCodes, $priceTicket)
{
	$amount = count($ticketCodes) * $priceTicket;
	$ticketsnumbers = implode(", ", $ticketCodes);

	$ticketsImg = '';
	$ticketsLink = '';

	foreach($ticketCodes as $ticketCode){
		$ticketsImg .= '<img src="'.URL.'gala/tickets/'.$ticketCode.'" alt="Gala Tickets NAC 2015 - ticket '.$ticketCode.'" style="page-break-before:always;"><br>';
		$ticketsLink .= '<a href="'.URL.'gala/tickets/'.$ticketCode.'">Download: '.URL.'gala/tickets/'.$ticketCode.'</a><br>';
	}

	$ticketsImg = '<p>'.$ticketsImg.'</p>';
	$ticketsLink = '<p>'.$ticketsLink.'</p>';

	$title = 'Order Confirmation - Gala Tickets';

	$headers =  "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$headers .= "From: NAC <ellie@nationaladvertisingchallenge.com>\r\n";
  $headers .= "Bcc: ellie@nationaladvertisingchallenge.com\r\n";

	$body = '
		<p>
			Thank you for purchasing tickets to the 2015 National Advertising Challenge Gala!
			Attached you will find your tickets which are needed for entry.
			Please print them and bring them to the gala. We look forward to seeing you there!
		</p><p>
			<strong>What</strong>: 2015 NAC Gala<br>
			<strong>When</strong>: April 30th, 2015<br>
			<strong>Where</strong>: 2nd Floor Events, <a href="https://goo.gl/maps/20esj">461 King St W, Toronto, ON M5V 1K4</a><br>
		</p><br>

		<h3>Order summary</h3>
		<p>
			---------------------------------------------<br/>
			<strong>Date:</strong> '.date("F j, Y").'<br/>
			<strong>Email:</strong> '.$email.'<br/>
			<strong>Tickets Numbers:</strong> '.$ticketsnumbers.'<br/>
			<br/>
			<strong>Cost:</strong> $'.$amount.'<br/>
			<strong>HST(13%):</strong> $'.($amount * 0.13).'<br/>
			<strong>Total:</strong> $'.($amount * 1.13).'<br/>
			<strong>Billed On:</strong> '.$charge['source']['brand'].' .... '.$charge['source']['last4'].'<br/>
			<br/>
			<strong>Billed To:</strong> '.$charge['source']['name'].'<br/>
			<strong>Address:</strong><br/>
			'.$charge['source']['address_line1'].'<br/>
			'.$charge['source']['address_city'].' '.$charge['source']['address_state'].',	'.$charge['source']['address_zip'].'<br/>
			'.$charge['source']['address_country'].'<br/>
			<br/>
			<strong>Billed From:</strong> National Advertising Challenge<br/>
			<strong>Address:</strong><br/>
			100 Miranda Av., 2nd floor <br/>
			Toronto ON, M6B 3W7<br/>
			Canada<br/>
			---------------------------------------------<br/><br/>
		</p>

		<h3>Tickets</h3>
		'.$ticketsLink.$ticketsImg;

	$body .= emailSignature();

	return mail($email, $title, $body, $headers);
}


// ================================================================================= //
// Email Confirmation
// Arguments:
//  - email: email address of the payer
//  - the object content with all the information from Stripe
// ================================================================================= //

function emailConfirmation($email, $entryInfo, $amount, $charge = null)
{
	$bill = '';
	if(!is_null($charge)){
		$bill = '
			<br/>
			<strong>Cost:</strong> $'.$amount.'<br/>
			<strong>HST(13%):</strong> $'.($amount * 0.13).'<br/>
			<strong>Total:</strong> $'.($amount * 1.13).'<br/>
			<br/>
			<strong>Billed On:</strong> '.$charge['source']['brand'].' .... '.$charge['source']['last4'].'<br/>
			<strong>Billed To:</strong><br/>
			'.$email.'<br/>
			'.$entryInfo['E1_firstName'].' '.$entryInfo['E1_lastName'].'<br/>
			'.$entryInfo['E1_address1'].'<br/>
			'.($entryInfo['E1_address2'] != '' ? $entryInfo['E1_address2'].'<br/>' : '').'
			'.$entryInfo['E1_city'].', '.$entryInfo['E1_province'].' '.$entryInfo['E1_postalCode'].'<br/>
			Canada<br/>
		';
	}

	$title = 'Confirmation - '.$entryInfo['category'].' - '.$entryInfo['entryCode'].'';

	$headers =  "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$headers .= "From: NAC <ellie@nationaladvertisingchallenge.com>\r\n";
	$headers .= "Bcc: ellie@nationaladvertisingchallenge.com\r\n";

	$body = '
		<h2>Thank you for entering NAC 2015!</h2>

		<p>
			Congratulations!<br/>
			You have successfully completed your submission.
		</p>

		<h3>Order summary</h3>
		<p>
			---------------------------------------------<br/>
			<strong>Date:</strong> '.date("F j, Y").'<br/>
			<strong>Category:</strong> '.$entryInfo['category'].'<br/>
			<strong>Entry ID:</strong> '.$entryInfo['entryCode'].'<br/>
			'.$bill.'
			---------------------------------------------<br/><br/>
		</p><p>
			<strong>
				Additional entries may be submitted at a reduced cost of $50 (all categories).
			</strong>
		</p><p>
			Don’t forget the Gala is April 30th, 2015. We have exciting prizes, entertainment and delicious eats!
			<br/>
			Tickets are available for <a href="https://nationaladvertisingchallenge.com/gala">purchase online</a> - a limited number will be available at the door.
		 </p><p>
			To view your submissions, log in to your account at <a href="https://nationaladvertisingchallenge.com">nationaladvertisingchallenge.com</a>.
		</p>
	';

  $body .= emailSignature();

	mail($email, $title, $body, $headers);
}


// ================================================================================= //
// Email forgot password
// Arguments:
//  - email: email address we try to verify
//  - verificationNumber: number send to the user to compare
// ================================================================================= //

function emailPassword($DBlink, $email)
{

	$title = 'How to change your password on NAC';

	$headers =  "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$headers .= "From: NAC <ellie@nationaladvertisingchallenge.com>\r\n";

	$verificationNumber = getEmailVerif($DBlink, $email);

	$body = '
		<p>
			Hello,<br/><br/>
			You requested that we help you log in to NAC. If you did not request this email, you can ignore it -- your password has not been changed.
		</p><p>
			Forgot your password? -- <a href="'.URL.'forget-password/'.$email.'/'.$verificationNumber.'">click here to change your password</a><br/>
			<br/>
			If clicking the link does not work, copy and paste the entire address below into your browser!<br/>
			<br/>
			'.URL.'forget-password/'.$email.'/'.$verificationNumber.'<br/>
			<br/>
			You will be asked to change your password.
		</p>
	';

  $body .= emailSignature();

	mail($email, $title, $body, $headers);
}
