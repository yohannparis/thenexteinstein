<?php

// ---- Create an Account
class createAccount {

	function get(){

		// Redirect to the dashboard if logged
		global $logged;
		if($logged){
			header("Location: ".URL."dashboard");
			exit;
		}

		// Variables
		$page = 'create-account';
		$description = 'Create an Account';
		$seo_description = "Rules were meant to be broken. Except for these ones. Enter the 2014 NAA now.";
		include 'view/template.php';
	}

	function post(){

		global $logged;
		global $DBlink;

		// ---- The user wants to register
		if (isset($_POST['email']) and isset($_POST['password']) and isset($_POST['submit']))
		{

			// Test if the value aren't empty and email is an email
			if ($_POST['email'] != '' and $_POST['password'] != '')
			{

				// If the user exist we initialise the Session
				if (existUser($DBlink, $_POST['email'], cleanOutput($_POST['password'])))
				{
					// Access to the database to get the information
					$userInformation = userInformation($DBlink, $_POST['email'], $_POST['password']);
					$_SESSION['id'] = $userInformation['id'];
					$_SESSION['email'] = $userInformation['email'];
					$_SESSION['isLogin'] = true;    // Set the user is login
					$logged = true;
				}

				// If the user already exist, but with the wrong password
				else if(existUser($DBlink, $_POST['email'])) { $msg = 'ERROR_LOG_IN'; }

				// Else we register him classicaly
				else {

					// Test if the user agree to the Privacy Policy and Terms and Conditions
					if (isset($_POST['approval'])){

						// Register the user
						if (registerUser($DBlink, $_POST['email'], cleanOutput($_POST['password']))){
							// Access to the database to get the information
							$userInformation = userInformation($DBlink, $_POST['email'], $_POST['password']);
							$_SESSION['id'] = $userInformation['id'];
							$_SESSION['email'] = $userInformation['email'];
							$_SESSION['isLogin'] = true;    // Set the user is login
							$logged = true;


							if(isset($_POST['newsletter'])){
								// Get Mailchimp PHP API
								include 'vendor/mailchimp.class.php';
								$mailChimp = new MailChimp('bca65ad36d43585e81304cedbf8f3edd-us2');

								// Subscribe user to our newsletter
								$result = $mailChimp->call('lists/subscribe', array(
									'id'                => '2becc38e7a',
									'email'             => array('email'=>$_POST['email']),
									'double_optin'      => false,
									'update_existing'   => true,
									'send_welcome'      => false,
								));
							}
						}

						// Did not succeeded
						else { $msg = 'ERROR_REGISTER'; }
					}

					// Did not agree to the Privacy Policy and Terms and Conditions
					else { $msg = 'ERROR_AGREE'; }
				}
			}

			// End test value are empty
			else { $msg = 'ERROR_INFORMATION'; }

		} // End of test Login or register

		// ---- Succesfully logged in to the Dashboard
		if($logged){
			// Redirect to the dashboard
			header("Location: /dashboard");
			exit;
		}

		// ----- Did not logged in
		else {
			// Variables
			$page = 'create-account';
			$description = 'Create an Account';
			$seo_description = "Rules were meant to be broken. Except for these ones. Enter the 2014 NAA now.";
			include 'view/template.php';
		}
	}
}

// ---- Dashboard
class dashboard {

	function get() {

		global $logged;

		// Succesfully logged in to the Dashboard
		if($logged){

			cleanSession();
			global $DBlink;

			include 'model/2015/category.php';

			// Variables
			$page = 'dashboard';
			$section = 'dashboard';
			$description = 'Dashboard';
			include 'view/template.php';
		}

		// Did not logged in or logged out, so we send him back to the competition page
		else {
			// Redirect to the dashboard
			header("Location: /briefs");
			exit;
		}
	}

	function post() {

		// Succesfully logged in to the Dashboard
		global $logged;
		if($logged){

			cleanSession();

			// Variables
			$page = 'dashboard';
			$section = 'dashboard';
			$description = 'Dashboard';
			include 'view/template.php';
		}

		else {
			// Redirect to the dashboard
			header("Location: /briefs");
			exit;
		}
	}
}

// ---- Dashboard Create new submission
class dashboardCreate {

	function get() {

		global $logged;

		// Succesfully logged in to the Dashboard
		// And before the deadline on March 31 at 6am.
		if($logged and date('YmdH') < '2015033106'){

			cleanSession();
			global $DBlink;

			/* ==== Entry Creation ==== */

			/*  I use the entryCode as a unique ID for each entry application started by the user.
			 *  I save this information into the Session to know where we at at anytime.
			 *  When the user goes into the dashboard or arrive to Step 6 we clean every trace of the
			 *  entry information from the Session.
			 */

			// {0} Step 0 - From the Dashboard
			$step0 = step0($DBlink);

			// We succeeded, so let's go back to the Dashboard shall we
			if($step0){
				header("Location: /dashboard");
			}

			// Variables
			$page = 'dashboard';
			$section = 'dashboard';
			$description = 'Dashboard';
			include 'view/template.php';
		}

		// Did not logged in or logged out, so we send him back to the competition page
		else {
			// Redirect to the dashboard
			header("Location: /briefs");
			exit;
		}
	}
}

// ---- Dashboard login
class dashboardLogin {

	function post(){

		global $DBlink;
		$success = false;

		// Get the value
		$dashboardEmail = $_POST['email'];
		$dashboardPassword = $_POST['password'];

		// Test if the value aren't empty and email is an email
		if ($dashboardEmail != '' and $dashboardPassword != ''){

			// If the user exist we initialise the Session
			if (existUser($DBlink, $dashboardEmail, cleanOutput($dashboardPassword))){
				// Access to the database to get the information
				$userInformation = userInformation($DBlink, $dashboardEmail, $dashboardPassword);
				$_SESSION['id'] = $userInformation['id'];
				$_SESSION['email'] = $userInformation['email'];
				$_SESSION['isLogin'] = true;    // Set the user is login
				$msg = 'SUCCESS';
				$success = true;
			}

			// If the user already exist, but with the wrong password
			else if(existUser($DBlink, $dashboardEmail)) { $msg = 'ERROR_LOG_IN'; }

			// Else the user do not exist
			else { $msg = 'ERROR_DO_NOT_EXIST'; }
		}

		// End test value are empty
		else { $msg = 'ERROR_INFORMATION'; }

		// Return result
		header('Content-Type: application/json');
		echo json_encode(array('sent' => $success, 'message' => $msg, 'email' => $dashboardEmail, 'password' => $dashboardPassword));
	}
}

// ---- Delete entry
class deleteEntry {

	function get($entryCode) {

		// Succesfully logged in to the Dashboard
		global $logged;
		if($logged){

			global $DBlink;
			deleteEntryUser($DBlink, $entryCode, $_SESSION['id']);

			cleanSession();

			// Variables
			$page = 'dashboard';
			$seo_title = 'Dashboard';
			include 'view/template.php';
		}

		// Did not logged in or logged out, so we send him back to the competition page
		else {
			// Redirect to the dashboard
			header("Location: /briefs");
			exit;
		}
	}
}

// ---- Log out
class logout {
	function get() {

		// Empty the Session
		$_SESSION = array();

		// Delete all information in the Cookies
		if (ini_get("session.use_cookies"))
		{
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
		}

		// Destroy the Session
		session_destroy();
		header("Location: /briefs");
	}
}
