<?php

class submit {

	function get($step = null, $entryCode = null){

		// Redirect unlogged persons.
		global $logged;
		if(!$logged){
			header('Location: /briefs');
		}

		require_once 'controller/entries.php';
		global $DBlink;
		global $priceFull;
		global $priceDiscount;
		global $priceStudent;

		/* ==== Steps ==== */

			// If no step are given, let's go to the first one
			if (is_null($step)){ $step = 1; }

			// If an entry code is given
			if (!is_null($entryCode)){

				// Setup the Session value to this one
				$_SESSION['entryCode'] = $entryCode;

				// Get the step process of the entry
				$stepProcess = stepProcessEntryUser($DBlink, $_SESSION['entryCode'], $_SESSION['id']);

				// Check if try to access the step where the entry shouldn't go
				// Try to access another step
				if($step != $stepProcess){

					// Are we before payment?
					if($stepProcess < 5){

						// Does the user try to access a page posterior that his entry step
						if($step > $stepProcess){
							$step = $stepProcess;
						}

					// Then we send back the user to after the review
					} else {
						$step = $stepProcess;
					}
				}
			}

		/* ==== Price ==== */

			// In the case the user already completed one entry
			// the new price is setup to discount.
			$discount = false;
			if(existCompleteEntryUser($DBlink, $_SESSION['id']))
			{
				$priceFull = $priceDiscount;
				$discount = true;
			}

			// Set the price to high price
			$price = $priceFull;

			// Setup the price to Young or Student
			if($category === 'student'){
				$price = $priceStudent;
			}

			$hst = $price * 0.13;
			$total = $price * 1.13;

		/* ==== Page ==== */

			$page = 'step'.$step;
			$section = 'submit';
			$description = 'Submission '.$page;
			include 'view/template.php';
	}

	function post($step = null, $entryCode = null){

		// Redirect unlogged persons.
		global $logged;
		if(!$logged){
			header('Location: '.URL.'briefs');
		}

		require_once 'controller/entries.php';
		global $DBlink;
		global $priceFull;
		global $priceDiscount;
		global $priceStudent;

		/* ==== Steps ==== */

			// If no step are given, let's go to the first one
			if (is_null($step)){ $step = 1; }

			// If an entry code is given
			if (!is_null($entryCode)){

				// Setup the Session value to this one
				$_SESSION['entryCode'] = $entryCode;

				// Get the step process of the entry
				$stepProcess = stepProcessEntryUser($DBlink, $_SESSION['entryCode'], $_SESSION['id']);

				// Check if try to access the step where the entry shouldn't go
				// Try to access another step
				if($step != $stepProcess){

					// Are we before payment?
					if($stepProcess < 5){

						// Does the user try to access a page posterior that his entry step
						if($step > $stepProcess){
							$step = $stepProcess;
						}

					// Then we send back the user to after the review
					} else {
						$step = $stepProcess;
					}
				}
			}

		/* ==== Price ==== */

			// In the case the user already completed one entry
			// the new price is setup to discount.
			$discount = false;
			if(existCompleteEntryUser($DBlink, $_SESSION['id']))
			{
				$priceFull = $priceDiscount;
				$discount = true;
			}

			// Set the price to high price
			$price = $priceFull;

			// Setup the price to Young or Student
			if($category === 'student'){
				$price = $priceStudent;
			}

			$hst = $price * 0.13;
			$total = $price * 1.13;

		/* ==== Page ==== */

			$page = 'step'.$step;
			$section = 'submit';
			$description = 'Submission '.$page;
			include 'view/template.php';
	}
}
