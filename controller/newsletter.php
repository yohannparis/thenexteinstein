<?php

// Get Mailchimp PHP API
include 'vendor/mailchimp.class.php';

// Get the info
$email = $_REQUEST['email'];

// Connect with the API key
$mailChimp = new MailChimp('bca65ad36d43585e81304cedbf8f3edd-us2');

// Subscribe user to our newsletter
// http://apidocs.mailchimp.com/api/2.0/lists/subscribe.php
$result = $mailChimp->call('lists/subscribe', array(
	'id'                => '2becc38e7a',
	'email'             => array('email'=>"$email"),
	'double_optin'      => false,
	'update_existing'   => true,
	'send_welcome'      => false,
));

// In case of error we notify the developer, and notify the user
if (isset($result['status'])){
	mail('developer@metricksystem.com', 'NAC 2015 - Optin error', $email);
	$message = array('sent' => false, 'msg' => "Error, please verify your email or refresh your browser.");

// If the person have been added correctly
} else {
	$message = array('sent' => true, 'msg' => "Thank you for subscribing to our newsletter!");
}

// Return the result in a JSON format
header('Content-type: application/json');
echo json_encode($message);
