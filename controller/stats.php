<?php

// ---- Stats of brief download
function addBriefDownloadStat($DBlink, $category)
{
	$request = "INSERT INTO stats (stat, info) VALUES ('download-brief', '$category');";
	return mysqli_query($DBlink, $request);
}

// ---- Get all the briefs downloaded per category
function getStatsBriefs($DBlink, $info)
{
	$request = "SELECT info, count(*) AS sum FROM stats WHERE stat LIKE '$info' GROUP BY info ORDER BY info ASC;";
	$result = mysqli_query($DBlink, $request);
	$results = array();
	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
		$results[$row['info']] = $row['sum'];
	}
	return $results;
}

// ---- Get all the briefs downloaded
function getStatsBriefsTotal($DBlink, $info)
{
	$request = "SELECT * FROM stats WHERE stat LIKE '$info';";
	$result = mysqli_query($DBlink, $request);
	return mysqli_num_rows($result);
}


// ---- Get all the entries done or in progress by the user
function getEntriesStats($DBlink)
{
	$request = "SELECT category, count(*) AS sum FROM entries WHERE stepProcess > 2 AND stepProcess != 6 GROUP BY category ORDER BY category ASC;";
	$result = mysqli_query($DBlink, $request);
	$results = array();
	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
		$results[$row['category']] = $row['sum'];
	}
	return $results;
}

// ---- Get all the entries in progress
function getEntriesTotal($DBlink)
{
	$request = "SELECT * FROM entries WHERE stepProcess != 6 ;";
	$result = mysqli_query($DBlink, $request);
	return mysqli_num_rows($result);
}

// ---- Get all the entries done and paid by the user
function getEntriesPaid($DBlink)
{
	$request = "SELECT category, count(*) AS sum FROM entries WHERE stepProcess = 6 GROUP BY category ORDER BY category ASC;";
	$result = mysqli_query($DBlink, $request);
	$results = array();
	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
		$results[$row['category']] = $row['sum'];
	}
	return $results;
}

// ---- Get all the entries paid
function getEntriesPaidTotal($DBlink)
{
	$request = "SELECT * FROM entries WHERE stepProcess = 6 AND category != 'student';";
	$result = mysqli_query($DBlink, $request);
	return mysqli_num_rows($result);
}

// ---- Get all the New Registration based on time
function getRegistration($DBlink, $year){
	$request = "SELECT * FROM users WHERE users.date > '$year'";
	$result = mysqli_query($DBlink, $request);
	if (mysqli_num_rows($result) > 0){ return mysqli_num_rows($result); }
	else { return 0; }
}

// ---- Get all the Gala tickets
function getGalaTickets($DBlink){
	$request = "SELECT * FROM tickets";
	$result = mysqli_query($DBlink, $request);
	if (mysqli_num_rows($result) > 0){ return mysqli_num_rows($result); }
	else { return 0; }
}

// ---- Get all the Gala tickets
function getGalaTicketsInfo($DBlink){
	$request = "SELECT email, code, date FROM tickets ORDER BY email ASC";
	$result = mysqli_query($DBlink, $request);
	$results = array();
	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
		array_push($results, $row);
	}
	return $results;
}
