<?php
/*
*  This file setup the users information access and everything linked to him.
*  This regrouped all the intelligence of the user and getter and setter as well.
*/

// ---- User exist, email already used?
function existUser($DBlink, $email, $password = null)
{
	// If the user try to connect
	if ($password != null){ $request = "SELECT * FROM users WHERE email = '".$email."' AND password = '".md5(SALT.$password)."';"; }

	// Or just want to check if the email exist
	else { $request = "SELECT * FROM users WHERE email = '".$email."'"; }

	$result = mysqli_query($DBlink, $request);
	if (mysqli_num_rows($result) > 0){ return true; }
	else { return false; }
}


// ---- Register User
function registerUser($DBlink, $email, $password)
{

	$passwordMD5 = md5(SALT.$password); // Make a MD5 password with SALT word
	$code        = md5(SALT.$email);    // Make a MD5 code with the email and the SALT word

	$request = "INSERT INTO users (email, password, date, code) VALUES ('$email', '$passwordMD5', SYSDATE(), '$code');";

	if (mysqli_query($DBlink, $request)){ return true; }
	else { return false; }
}

// ---- Get all the information
function userInformation($DBlink, $email, $password)
{
	$request = "SELECT * FROM users WHERE email = '".$email."' AND password = '".md5(SALT.$password)."';";
	$result = mysqli_query($DBlink, $request);
	return mysqli_fetch_array($result, MYSQLI_ASSOC);
}

// ---- Get the email verification code to send an email to the user
function getEmailVerif($DBlink, $email)
{
	$requestUser = "SELECT code FROM users WHERE email = '".$email."';";
	$resultUser = mysqli_query($DBlink, $requestUser);
	$result = mysqli_fetch_array($resultUser, MYSQLI_ASSOC);
	return $result['code'];
}

// ---- Change the Password
function changePassword($DBlink, $email, $verificationNumber, $newPassword)
{
	$request = "UPDATE users SET password = '".md5(SALT.$newPassword)."' WHERE email = '".$email."' AND code = '".$verificationNumber."';";
	$result = mysqli_query($DBlink, $request);
	return $result;
}

// ---- Get all the entries done or in progress by the user
function userEntries($DBlink, $userID)
{
	$request = "SELECT category, stepProcess, date, modified, entryCode FROM entries WHERE userID = '".$userID."' ORDER BY date ASC;";

	$result = mysqli_query($DBlink, $request);
	$results = array();

	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
		array_push($results, $row);
	}

	return $results;
}

// ---- Get all the upload of one entry of the user
function getUploads($DBlink, $entryCode, $userID)
{
	$request = "SELECT * FROM uploads WHERE userID = $userID. AND entryCode = '$entryCode' ORDER BY date ASC;";
	$result = mysqli_query($DBlink, $request);
	$results = array();

	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
		array_push($results, $row);
	}

	return $results;
}

// ---- Get all the information from the entry
function entryInformation($DBlink, $entryCode, $userID = null)
{
	$requestUserID = '';
	if (!is_null($userID)) { $requestUserID = "AND userID = '$userID'"; }
	$request = "SELECT * FROM entries WHERE entryCode = '$entryCode' $requestUserID;";
	$result = mysqli_query($DBlink, $request);
	return mysqli_fetch_array($result, MYSQLI_ASSOC);
}

// ---- Test if an entryCode is valid with an user.
function existEntryUser($DBlink, $entryCode, $userID)
{
	$request = "SELECT * FROM entries WHERE userID = '".$userID."' AND entryCode = '".$entryCode."';";
	$result = mysqli_query($DBlink, $request);
	$results = array();
	if (mysqli_num_rows($result) > 0){ return true; }
	else { return false; }
}

// ---- Test if one of the entries submitted by the user is completed.
function existCompleteEntryUser($DBlink, $userID)
{
	$request = "SELECT * FROM entries WHERE userID = '".$userID."' AND stepProcess = 6;";
	$result = mysqli_query($DBlink, $request);
	if (mysqli_num_rows($result) > 0){ return true; }
	else { return false; }
}

// ---- Get the Step process of an entry submitted by an user.
function stepProcessEntryUser($DBlink, $entryCode, $userID)
{
	$request = "SELECT stepProcess FROM entries WHERE userID = '$userID' AND entryCode = '$entryCode';";
	$result = mysqli_query($DBlink, $request);
	$result = mysqli_fetch_array($result, MYSQLI_ASSOC);
	return $result['stepProcess'];
}

// ----  Delete an entry of an user.
function deleteEntryUser($DBlink, $entryCode, $userID)
{
	$request = "DELETE FROM entries WHERE userID = '".$userID."' AND entryCode = '".$entryCode."';";
	$result = mysqli_query($DBlink, $request);
	if ($result){ return true; }
	else { return false; }
}
