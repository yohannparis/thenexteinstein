<?php

function categoriesStatsDisplay($DBlink){
	/*
	$statsEntries = getEntriesStats($DBlink);
	$statsPaid = getEntriesPaid($DBlink);
	$graphEntries = $graphPaid = '';

	foreach ($statsEntries as $key => $value) {
		$graphEntries .= '{"title" : "'.$key.'", "value" : '.$value.'},';
	}

	foreach ($statsPaid as $key => $value) {
		$graphPaid .= '{"title" : "'.$key.'", "value" : '.$value.'},';
	}

	header('Content-Type: application/json');
	echo '{
			"graph" :
			{
				"title" : "NAC 2015 - Categories",
				"total" : false,
				"datasequences" : [
					{
						"title": "Entries",
						"refreshEveryNSeconds" : 30,
						"color": "lightGray",
						"datapoints" : ['.$graphEntries.']
					},
					{
						"title": "Paid",
						"refreshEveryNSeconds" : 30,
						"color": "green",
						"datapoints" : ['.$graphPaid.']
					}
				]
			}
		}';

	*/

	$statsPaid = getEntriesPaid($DBlink);
	$graphPaid = '';

	$color['ooh'] = 'red';
	$color['digital'] = 'green';
	$color['campaign'] = 'aqua';
	$color['media'] = 'purple';
	$color['wild-card'] = 'mediumGray';
	$color['big-ideas'] = 'orange';
	$color['print'] = 'pink';
	$color['not-for-profit'] = 'yellow';
	$color['student'] = 'blue';

	foreach ($statsPaid as $key => $value) {
		$graphPaid .= '
			{
				"title": "'.$key.'",
				"refreshEveryNSeconds" : 30,
				"color": "'.$color[$key].'",
				"datapoints" : [
					{
						"title" : "'.$key.'",
						"value" : '.$value.'
					}
				]
			},
		';
	}

	header('Content-Type: application/json');
	echo '{"graph": {"title": "NAC 2015 - Application Submitted", "total": false, "datasequences": ['.$graphPaid.'] }}';
}

function briefsStatsDisplay($DBlink){

	$statsBriefs = getStatsBriefs($DBlink, 'download-brief');
	$graphBriefs = '';

	$color['ooh'] = 'red';
	$color['digital'] = 'green';
	$color['campaign'] = 'aqua';
	$color['media'] = 'purple';
	$color['wild-card'] = 'mediumGray';
	$color['big-ideas'] = 'orange';
	$color['print'] = 'pink';
	$color['not-for-profit'] = 'yellow';
	$color['student'] = 'blue';

	foreach ($statsBriefs as $key => $value) {
		$graphBriefs .= '
			{
				"title": "'.$key.'",
				"refreshEveryNSeconds" : 30,
				"color": "'.$color[$key].'",
				"datapoints" : [
					{
						"title" : "'.$key.'",
						"value" : '.$value.'
					}
				]
			},
		';
	}

	header('Content-Type: application/json');
	echo '{"graph": {"title": "NAC 2015 - Briefs", "total": false, "datasequences": ['.$graphBriefs.'] }}';
}

function panicStatusboardHTML($info, $number, $description, $refresh, $color = null){

	if(is_null($color)){ $color = 'white'; }

	return '
		<!DOCTYPE html><html><head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8">
		<meta http-equiv="Cache-control" content="no-cache">
		<meta application-name="'.$description.'" data-allows-resizing="NO" data-default-size="4,2" data-allows-scrolling="NO">
		<style type="text/css">
			* { margin: 0; padding: 0; }
			html, body { overflow: hidden; background: transparent; font-size: 20px; }
			body { color: white; font-family: \'Roadgeek2005SeriesD\', sans-serif; width: 250px; height: 128px; text-align: center; }
			h1, h2, h3 { font-weight: normal; text-transform: uppercase; }
			h1 { font-size: 5em; line-height: .75em; padding-top: 0.25em; color: '.$color.';}
			h2 { font-size: .75em; line-height: 1em; color: #7e7e7e; }

			/* Dev only
				@font-face { font-family: "Roadgeek2005SeriesD"; src: url("http://panic.com/fonts/Roadgeek 2005 Series D/Roadgeek 2005 Series D.otf"); }
				body { background: black; border: 1px solid; }
			*/
		</style>
		<script type="text/javascript">
			function refresh(){
				var req = new XMLHttpRequest();
				req.onreadystatechange=function() {
					if (req.readyState==4 && req.status==200) { document.getElementById("howmany").innerText = req.responseText; }
				}
				req.open("POST", "http://nac.dev.10.0.0.127.xip.io/stats/'.$info.'", true);
				req.send(null);
			}
			function init() { refresh(); var int=self.setInterval(function(){refresh()},'.$refresh.'000); }
		</script>
		</head><body onload="init()"><h1 id="howmany">'.$number.'</h1><h2>'.$description.'</h2></body></html>
	';
}

class statsDisplay {
	function get($info){

		// Get the database
		global $DBlink;

		switch ($info) {
			case 'categories':
				categoriesStatsDisplay($DBlink);
				break;

			case 'application-started':
				$stats = getEntriesTotal($DBlink);
				echo panicStatusboardHTML($info, $stats, 'Application Started', 30);
				break;

			case 'application-paid':
				$stats = getEntriesPaidTotal($DBlink);
				echo panicStatusboardHTML($info, $stats, 'Application Paid', 30, '#00C200');
				break;

			case 'briefs':
				briefsStatsDisplay($DBlink);
				break;

			case 'briefs-total':
				$stats = getStatsBriefsTotal($DBlink, 'download-brief');
				echo panicStatusboardHTML($info, $stats, 'Briefs Downloaded', 30);
				break;

			case 'new-registration':
				$stats = getRegistration($DBlink, 2015);
				echo panicStatusboardHTML($info, $stats, 'Registration', 30);
				break;

			case 'gala-tickets':
				$stats = getGalaTickets($DBlink);
				echo panicStatusboardHTML($info, $stats, 'Gala Tickets', 30);
				break;

			case 'gala-tickets-info':
				$stats = getGalaTicketsInfo($DBlink);
				header('Content-Type: application/excel');
				header('Content-Disposition: attachment; filename="gala-tickets.csv"');
				$fp = fopen('php://output', 'w');
				foreach ($stats as $line) {
				 	fputcsv($fp, $line);
				 }
				fclose($fp);
				break;

			case 'entrance-close':
				$today = new DateTime("now");
				$end = date_create('2015-03-31');
				$stats = date_diff($today, $end);
				echo panicStatusboardHTML($info, $stats->format('%a'), 'days left', 3600);
				break;
		}
	}

	function post($info){

		// Get the database
		global $DBlink;

		switch ($info) {
			case 'application-started':
				echo getEntriesTotal($DBlink);
				break;

			case 'application-paid':
				echo getEntriesPaidTotal($DBlink);
				break;

			case 'briefs-total':
				echo getStatsBriefsTotal($DBlink, 'download-brief');
				break;

			case 'new-registration':
				echo getRegistration($DBlink, 2015);
				break;

			case 'gala-tickets':
				echo getGalaTickets($DBlink);
				break;

			case 'entrance-close':
				$today = new DateTime("now");
				$end = date_create('2015-03-31');
				$stats = date_diff($today, $end);
				echo $stats->format('%a');
				break;
		}
	}
}

class statsText {
	function post(){
		// Get the database
		global $DBlink;
		$today = new DateTime("now");
		$end = date_create('2015-03-31');
		$stats = date_diff($today, $end);

		echo '<?xml version="1.0" encoding="UTF-8" ?>
		<Response>
			<Message>
- '.$stats->format('%a').' days left
- '.getStatsBriefsTotal($DBlink, 'download-brief').' briefs DL
- '.getRegistration($DBlink, 2015).' accounts
- '.getEntriesTotal($DBlink).' new entries
- '.getEntriesPaidTotal($DBlink).' paid entries
- '.getGalaTickets($DBlink).' gala tickets

'.date('l F jS').' at '.date('g:ia').'
			</Message>
		</Response>';
	}
}
