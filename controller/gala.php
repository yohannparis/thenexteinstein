<?php

class gala {

	function get(){

		global $DBlink;
		global $priceTicket;
		global $maxTickets;

		// ---- Gala Ticket - Limit of tickets sale
		function getTicketsSold($DBlink){
			$result = mysqli_query($DBlink, "SELECT * FROM `tickets`;");
			return mysqli_num_rows($result);
		}

		$page = 'gala';
		$section = 'gala';
		$description = 'Gala';
		include 'view/template.php';
	}

	function post(){

		// ---- Gala Ticket - Payment
		require_once 'vendor/Stripe/Stripe.php';
		function buyTickets($token, $email, $ticketNumber, $amount)
		{
			/* Stripe Keys
				See your keys here https://manage.stripe.com/account
				Test: sk_test_7tQ6amoQ3qJt5GxZWgSgCcDd
				Live: sk_live_SPRZNEpuEpU3CCUobJrkca20
			*/
			Stripe::setApiKey("sk_live_SPRZNEpuEpU3CCUobJrkca20");

			// create the charge on Stripe's servers - this will charge the user's card
			try {
				$charge = Stripe_Charge::create(array(
					"amount" => ($ticketNumber*$amount*1.13)*100, // amount in cents, again
					"currency" => "cad",
					"card" => $token,
					"description" => $email.' - '.$ticketNumber.' Gala tickets')
				);

				if($charge['paid']) {
					return $charge;
				} else return $charge['failure_message'];
			}

			catch(Stripe_CardError $e) {
				// Since it's a decline, Stripe_CardError will be caught
				$body = $e->getJsonBody();
				$error = $body['error'];

				if($error['code'] === 'card_declined'){
					mail('developer@metricksystem.com', 'NAC Gala - Card Error', "The card was declined - $email - $ticketNumber Gala tickets");
					return 'The card was declined.';
				}
				else if($error['code'] === 'expired_card'){
					mail('developer@metricksystem.com', 'NAC Gala - Card Error', "The card has expired - $email - $ticketNumber Gala tickets");
					return 'The card has expired.';
				}
				else {
					mail('developer@metricksystem.com', 'NAC Gala - Card Error', "The card information are incorrect - $email - $ticketNumber Gala tickets");
					return 'The card information are incorrect.';
				}
			}

			catch (Stripe_InvalidRequestError $e) {
				// Invalid parameters were supplied to Stripe's API
				mail('developer@metricksystem.com', 'NAC - Stripe Error - Invalid API Request', 'Invalid parameters were supplied to Stripe\'s API');
				return 'An error occurred while processing the card.';
			}

			catch (Stripe_AuthenticationError $e) {
				// Authentication with Stripe's API failed (maybe you changed API keys recently)
				mail('developer@metricksystem.com', 'NAC - Stripe Error - API authentification', 'Authentication with Stripe\'s API failed (maybe you changed API keys recently)');
				return 'An error occurred while processing the card.';
			}

			catch (Stripe_ApiConnectionError $e) {
				// Network communication with Stripe failed
				mail('developer@metricksystem.com', 'NAC - Stripe Error - API connection', ' Network communication with Stripe failed');
				return 'An error occurred while processing the card.';
			}

			catch (Stripe_Error $e) {
				// Display a very generic error to the user, and maybe send yourself an email
				mail('developer@metricksystem.com', 'NAC - Stripe Error - Generic', 'Display a very generic error ');
				return 'An error occurred while processing the card.';
			}

			catch (Exception $e) {
				// Something else happened, completely unrelated to Stripe
				mail('developer@metricksystem.com', 'NAC - Stripe Error - Unrelated', 'Something else happened, completely unrelated to Stripe');
				return 'An error occurred while processing the card.';
			}
		}

		// ---- Generate a unique number for the Gala ticket
		function genUniqueTicketNumber($len=5)
		{
			$hex  = md5(SALT.uniqid("", true));
			$pack = pack('H*', $hex);
			$tmp  = base64_encode($pack);
			$uid  = preg_replace("#(*UTF8)[^A-Z0-9]#", "", $tmp);
			return substr($uid, 0, $len);
		}

		// ---- Ticket Number
		function getTicketNumber($DBlink)
		{
			do {
				$uid = genUniqueTicketNumber();
				$result = mysqli_query($DBlink, "SELECT `code` FROM `tickets` WHERE `code` = '$uid';");
			} while (mysqli_num_rows($result) > 0);
			return $uid;
		}


		global $DBlink;
		global $priceTicket;
		global $maxTickets;

		$email = $_POST['token']['email'];
		$token = $_POST['token']['id'];
		$ticketsNB = $_POST['tickets'];

		// Return result
		header('Content-Type: text/html');
		// Access to the database to save the information
		$charge = buyTickets($token, $email, $ticketsNB, $priceTicket);

		// If buyTickets is a success we save the tickets in the database
		if ($charge['paid']){

			$request = "INSERT INTO tickets (tickets.date, tickets.email, tickets.code, tickets.price) VALUES ";
			$ticketCodes = array();

			for ($j=0; $j<$ticketsNB; $j++){

				// Create an array of Tickets Code for the email
				$ticketCodes[$j] = $ticketCode = getTicketNumber($DBlink);

				// Create a new entry
				$request .= "(SYSDATE(), '$email', '$ticketCode', '$priceTicket'),";
			}

			$request = substr($request, 0, -1).";";

			if (mysqli_query($DBlink, $request)) {
				$ticketsSent = emailGalaConfirmation($email, $charge, $ticketCodes, $priceTicket);

				if ($ticketsSent) {
					echo "You have successfully completed your order! <br>You will receive an email with the tickets and a receipt shortly.";
				}
				else {
					mail('developer@metricksystem.com', 'NAC - Gala Tickets - Email error', $email." as ordered ".$ticketsNB." tickets to the Gala, the payment has been approved (charge ID: ".$charge['id']." ), and the Tickets have been created on the Database. Please create send them to ".$email.".");
					echo "You have successfully completed your order! <br>But we were not able to send your tickets. Don't worry we are working on it and will send them in less than 48 hours.";
				}
			}
			else {
				mail('developer@metricksystem.com', 'NAC - Gala Tickets - DB error', $email." as ordered ".$ticketsNB." tickets to the Gala, the payment has been approved (charge ID: ".$charge['id']." ), but a database error didn't allow the creation of the tickets. Please create them and send them to ".$email.".");
				echo "You have successfully completed your order! <br>But we were not able to send your tickets. Don't worry we are working on it and will send them in less than 48 hours.";
			}
		}
		else { print_r($charge); }

	} // End post_xhr

} // End class

class ticket {

	function get($ticketNumber){

		global $DBlink;

		// Double check if the ticket number exist and in that case bring back the email of the buyer.
			$request = mysqli_query($DBlink, "SELECT `email` FROM `tickets` WHERE `code` = '$ticketNumber';");
			if (!$request or $ticketNumber === 'EXAMPLE'){
				$ticketNumber = 'EXAMPLE';
				$emailBuyer = 'email@test.com';
			} else {
				$result = mysqli_fetch_array($request, MYSQLI_ASSOC);
				$emailBuyer = $result['email'];
			}

		// Creating the ticket
			// 217 widht x 455 height
			// $image = imagecreatefrompng("model/2015/tickets/tickets.png");

			$ticketWidth = 600;
			$ticketHeight = 220;
			$ticket = imagecreate($ticketWidth, $ticketHeight);

		// Colors
			$white = imagecolorallocate($ticket, 255, 255, 255);
			$black = imagecolorallocate($ticket, 0, 0, 0);
			$accent = imagecolorallocate($ticket,44, 48, 137);

		// Border
			imagefill($ticket, 0, 0, $white);
			imagerectangle($ticket, 0, 0, $ticketWidth-1, $ticketHeight-1, $accent);

		// Adding the logo
			$logo = imagecreatefromjpeg("model/2015/tickets/logo.jpg");
			imagecopymerge($ticket, $logo, 125, 0, 0, 0, 220, 220, 100);
			imagedestroy($logo);

		// Name the font to be used
			$fontBlack = 'model/2015/tickets/Lato-Black.ttf';
			$fontBold = 'model/2015/tickets/Lato-Bold.ttf';
			$fontRegular = 'model/2015/tickets/Lato-Regular.ttf';
			$fontLight = 'model/2015/tickets/Lato-Light.ttf';

		// Text on the ticket
			imagettftext ($ticket, 16 , 0 , 355+9, 30, $black , $fontBold , 'Thurs. April 30th, 2015');
			imagettftext ($ticket, 14 , 0 , 355+45, 70, $black , $fontBold , '2nd Floor Events');
			imagettftext ($ticket, 10 , 0 , 355+53, 90, $black , $fontRegular , '461 King Street West');
			imagettftext ($ticket, 10 , 0 , 355+50, 108, $black , $fontRegular , 'Toronto, ON, M5V 1K4');
			imagettftext ($ticket, 10 , 0 , 355+45, 145, $black , $fontRegular , 'Exhibit opens at 6:30 PM');
			imagettftext ($ticket, 10 , 0 , 355+25, 163, $black , $fontRegular , 'Cocktails, awards ceremony and');
			imagettftext ($ticket, 10 , 0 , 355+50, 181, $black , $fontRegular , 'entertainment to follow');
			imagettftext ($ticket, 10 , 0 , 355+8, $ticketHeight-10, $accent , $fontRegular , 'nationaladvertisingchallenge.com/gala');

		// Email of the buyer
			imagettftext ($ticket, 10  , -90 , 75+20, 10, $black , $fontLight , $emailBuyer );

		// Ticket Number
			imagettftext ($ticket, 19 , -90 , 35, 75+10, $black , $fontBlack , $ticketNumber );

		// QR code
			include 'vendor/phpqrcode/qrlib.php';
			$codeContents = URL.'gala/checkin/'.$ticketNumber;
			$pixelPerPoint = 2;
			$jpegQuality = 100;

			// generating frame
			$frame = QRcode::text($codeContents, false, QR_ECLEVEL_M);

			// rendering frame with GD2
			$h = count($frame);
			$w = strlen($frame[0]);
			$base_image = imagecreate($w, $h);
			$white = imagecolorallocate($base_image, 255, 255, 255);
			$accent = imagecolorallocate($base_image,44, 48, 137);
			imagefill($base_image, 0, 0, $white);

			for($y=0; $y<$h; $y++) {
				for($x=0; $x<$w; $x++) {
					if ($frame[$y][$x] == '1') {
						imagesetpixel($base_image,$x,$y,$accent);
					}
				}
			}

			$qrcode = imagecreate($w * $pixelPerPoint, $h * $pixelPerPoint);
			imagecopyresized($qrcode, $base_image, 0, 0, 0, 0, $w * $pixelPerPoint, $h * $pixelPerPoint, $w, $h);
			imagedestroy($base_image);

		// Add the QRcode to the ticket
			imagecopymerge($ticket, $qrcode, 10, 10, 0, 0, $w * $pixelPerPoint, $h * $pixelPerPoint, 100);
			imagedestroy($qrcode);

		// Output the image has a png
			header("Content-type: image/png");
			imagepng($ticket);
			imagedestroy($ticket);
	}
}

class checkin {

	function get($ticketNumber){
		header('Location: /gala');
	}

	function post($ticketNumber){

		// Check in the ticket number
		global $DBlink;
		$request = mysqli_query($DBlink, "UPDATE `tickets` SET `checkin` = 1 WHERE `code` = '$ticketNumber';");

		// Return result has JSON
		header('Content-Type: text/json');
		if (!$request){
			echo '{msg: error}';

		} else {
			echo '{msg: success}';
		}
	}
}
