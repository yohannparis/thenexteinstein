<?php
/*
	Change password states:
		1 - form to enter email address. -> GET, error POST
		2 - Check if email correct and used by an user, then send the verification email. -> POST
		3 - User acces page from link from email, form to enter new password. -> GET, error POST
		4 - Password changed. -> POST
		5 - Password NOT changed. -> POST
 */

class forget_password {

	function get($email = null, $verifNumber = null) {

		// 1 - form to enter email address.
		$state = 1;

		// 3 - User acces page from link from email, form to enter new password
		if (!is_null($email) and !is_null($verifNumber)){
			$state = 3;
		}

		// ---- Display page
		$page = 'forget-password';
		$section = 'forget-password';
		$description = 'Forget Password';
		include 'view/template.php';

	} // end get()

	function post() {

		global $DBlink;

		// ---- Form Validation
		if (isset($_POST['email']) and !existUser($DBlink, $_POST['email']))
			{ $emailValidMsg['email'] = 'is not registered'; }
		if (isset($_POST['email']) and !is_valid_email_address($_POST['email']))
			{ $emailValidMsg['email'] = 'is not valid'; }
		if (isset($_POST['newPassword']) and (strlen($_POST['newPassword']) < 2))
			{ $passwordValidMsg['newPassword'] = '2 characters minimum'; }
		if (isset($_POST['newPassword2']) and ($_POST['newPassword'] != $_POST['newPassword2']))
			{ $passwordValidMsg['newPassword2'] = 'Passwords are different'; }


		// 1 - form to enter email address.
		if (isset($emailValidMsg) and isset($_POST['email']) and isset($_POST['sendEmail']) and $_POST['sendEmail'] == 'yes'){
			$state = 1;
		}

		// 2 - Check if email correct and used by an user, then send the verification email.
		if (!isset($emailValidMsg) and isset($_POST['email']) and isset($_POST['sendEmail']) and $_POST['sendEmail'] == 'yes'){
			emailPassword($DBlink, $_POST['email']);
			$state = 2;
		}

		// 3 - User acces page from link from email, form to enter new password
		if (isset($passwordValidMsg)){
			$state = 3;
		}

		// 4 - Password changed
		// 5 - Password NOT changed
		if (!isset($passwordValidMsg) and isset($_POST['email']) and isset($_POST['verifNumber']) and isset($_POST['newPassword'])){
			$passwordChanged = changePassword($DBlink, $_POST['email'], $_POST['verifNumber'], $_POST['newPassword']);
			if ($passwordChanged){ $state = 4; }
			else { $state = 5; }
		}


		// ---- Token for the main login form
		if(!isset($_SESSION['token'])){
			$_SESSION['token'] = md5(uniqid(mt_rand(), true));
		}


		// ---- Display page
		$page = 'forget-password';
		$section = 'forget-password';
		$description = 'Forget Password';
		include 'view/template.php';

	} // End post()
}
