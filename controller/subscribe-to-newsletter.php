<?php

// Get the info
$email = $_REQUEST['email'];

// Save the email by adding it to the list
try {
	//Open the CSV file
	$fp = fopen('../model/newsletter.csv', 'a');
	// Add a line
	fputcsv($fp, array("$email"));
	// Close the file
	fclose($fp);
	$message = array('sent' => true);

// In case of error
} catch( Exception $e){
	// We notify the developer
	mail('developer@metricksystem.com', 'Hebrew 2015 - Optin error', $email);
	// and notify the user
	$message = array('sent' => false);
}

// Return the result in a JSON format
header('Content-type: application/json');
echo json_encode($message);
