<?php

// ---- Brief download
class brief {
	function get($year, $category){

		// Check if the download was started by a browser
		$regexBrowser = '/^.*(msie|firefox|safari|webkit|opera|netscape|konqueror|gecko|chrome|googlebot|iphone|msnbot|applewebkit).*$/i';
		$userAgent = $_SERVER['HTTP_USER_AGENT'];

		if(preg_match($regexBrowser, $userAgent) and $year === '2015'){
			// Add download brief to the database
			global $DBlink;
			$stats = addBriefDownloadStat($DBlink, $category);
		}

		// Link to the file
		$pathToFile = "model/$year/brief/NAC-$year-$category.zip";

		// Define the rest of the header
		header('Content-Description: File Transfer '.$category);
		header('Content-Type: application/zip');
		header("Content-Transfer-Encoding: Binary");
		header('Content-Length: ' . filesize($pathToFile));
		header('Content-Disposition: attachment; filename=NAC-'.$year.'-'.$category.'.zip');

		readfile($pathToFile);
		exit;
	}
}

// ---- Simple PDF download for documents
class pdfs {
	function get($file) {

		// Link to the file
		$pathToFile = "model/2015/pdf/".$file;

		// Define the header
		header('Content-Description: File Transfer');
		header('Content-Type: application/pdf');
		header("Content-Transfer-Encoding: Binary");
		header('Content-Length: ' . filesize($pathToFile));

		// to open in browser
		//header('Content-Disposition: inline; filename=NAC-2015-'.$file);

		// to download
		header('Content-Disposition: attachment; filename=NAC-2015-'.$file);

		readfile($pathToFile);
	}
}
