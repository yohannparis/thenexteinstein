<?php
/* Router
 * Toro PHP - github.com/anandkunal/ToroPHP
 */
include 'vendor/Toro.php';

/* Handlers */
include 'download.php';
include 'winners.php';
//include 'newsletter.php';
include 'content-pages.php';
include 'forget-password.php';
include 'dashboard.php';
include 'submit.php';
include 'stats-display.php';

/* Errors */
ToroHook::add("404",  function() {
	$page = 'page404';
	include 'view/template.php';
});

/* URLs */
Toro::serve(array(

	// Submit Application
	"/submit" => "submit",
	"/submit/step([123456])" => "submit",
	"/submit/step([123456])/([a-zA-Z0-9]+)" => "submit",

	// Dashboard
	"/dashboard" => "dashboard",
	"/dashboard/login" => "dashboardLogin",
	"/dashboard/create" => "dashboardCreate",
	"/dashboard/delete/([a-zA-Z0-9]+)" => "deleteEntry",
	"/logout" => "logout",
	"/create-account" => "createAccount",

	// Forget Password
	"/forget-password" => "forget_password",
	"/forget-password/([a-zA-Z0-9.@-_]+)/([a-z0-9]+)" => "forget_password",

	// Document download
	"/([a-zA-Z0-9-_]+.pdf)" => "pdfs",
	"/brief/(201[3-4-5])/([a-z-]+)" => "brief",

	// Statistics
	"/stats/([a-z-]+)" => "statsDisplay",
	"/stats-sms" => "statsText",

	// Newsletter submission
	"/newsletter" => "newsletter",

	// Header Navigation
	"/" => "home",
	"/jury" => "the_jury",
	"/contact" => "contact",

	// Gala
	"/gala" => "gala",
	"/gala/tickets/([A-Z0-9]+)" => "ticket",
	"/gala/checkin/([A-Z0-9]+)" => "checkin",

	// Challenge
	"/about-the-challenge" => "challenge",
	"/categories" => "categories",
	"/prizing" => "prizing",

	// Entry + Briefs
	"/entry-information" => "entry_information",
	"/entry-specifications" => "entry_specifications",
	"/briefs" => "briefs",

	// Sponsorships
	"/sponsorships" => "sponsorships",
	"/([a-z-]+)sponsorships" => "sponsorships",

	// Winners
	"/winners" => "winners",

	// Footer Navigation
	"/legal" => "legal",
	"/privacy-policy" => "privacy_policy",
	"/terms-and-conditions" => "terms_and_conditions",
	"/rules-and-regulations" => "rules_and_regulations",
));
