<?php
/*
 * Include all the pages with no inteligence, just pure content.
 * They are goruped in order to keep the handlers class organised.
 */

/*
 * Header Navigation
 */

class home {
	function get(){
		$page = 'index';
		$section = 'index';
		$description = 'Home';
		include 'view/template.php';
	}
}

class the_jury {
	function get(){
		$page = 'jury';
		$section = 'jury';
		$description = 'Jury';
		$seo_description = "Meet our panel of the most accomplished creative and media directors from around Canada.";
		include 'view/template.php';
	}
}

class contact {
	function get(){
		$page = 'contact';
		$section = 'contact';
		$description = 'Contact';
		$seo_description = "Contact the National Advertising Awards.";
		include 'view/template.php';
	}
}

	// Challenge pages
	class challenge {
		function get(){
			$page = 'challenge';
			$section = 'challenge';
			$description = 'About The Challenge';
			include 'view/template.php';
		}
	}

	class categories {
		function get(){
			$page = 'challenge-categories';
			$section = 'challenge';
			$description = 'Categories';
			include 'view/template.php';
		}
	}

	class prizing {
		function get(){
			$page = 'challenge-prizing';
			$section = 'challenge';
			$description = 'Prizing';
			include 'view/template.php';
		}
	}

	// Sponsorships
	class sponsorships {

		function get($type = ''){

			switch ($type) {
				case 'media-':
					$page = 'sponsorship-media';
					$section = 'sponsorship';
					$description = 'Media Sponsorships';
					break;

				case 'brand-':
					$page = 'sponsorship-brand';
					$section = 'sponsorship';
					$description = 'Brand Sponsorships';
					break;

				case 'presenting-':
					$page = 'sponsorship-presenting';
					$section = 'sponsorship';
					$description = 'Presenting Sponsorships';
					break;

				case 'list-':
					$page = 'sponsorship-list';
					$section = 'sponsorship';
					$description = 'List of Sponsors';
					break;

				case 'info-':
					$page = 'sponsorship-info';
					$section = 'sponsorship';
					$description = 'Information about Sponsorships';
					break;

				default:
					$page = 'sponsorship';
					$section = 'sponsorship';
					$description = 'Sponsorships Overview';
					break;
			}

			include 'view/template.php';
		}
	}

	// Entry + Briefs
	class entry_information {
		function get(){

			global $priceStudent;
			global $priceFull;
			global $priceDiscount;

			$page = 'entry-information';
			$section = 'entry';
			$description = 'Entry Information';
			include 'view/template.php';
		}
	}

	class entry_specifications {
		function get(){
			$page = 'entry-specifications';
			$section = 'entry';
			$description = 'Entry Specifications';
			include 'view/template.php';
		}
	}

	class briefs {
		function get(){
			$page = 'briefs';
			$section = 'briefs';
			$description = 'Briefs';
			include 'view/template.php';
		}
	}

/*
 * Footer Navigation
 */

class legal {
	function get(){
		$page = 'legal';
		$section = 'legal';
		$description = 'Legal';
		include 'view/template.php';
	}
}

class privacy_policy {
	function get(){
		$page = 'privacypolicy';
		$section = 'privacypolicy';
		$description = 'Privacy Policy';
		include 'view/template.php';
	}
}

class terms_and_conditions {
	function get(){
		$page = 'terms';
		$section = 'terms';
		$description = 'Terms & Conditions';
		include 'view/template.php';
	}
}

class rules_and_regulations {
	function get(){

		global $priceStudent;
		global $priceFull;
		global $priceDiscount;

		$page = 'rulesregulations';
		$section = 'rulesregulations';
		$description = 'Rules & Regulations';
		include 'view/template.php';
	}
}
