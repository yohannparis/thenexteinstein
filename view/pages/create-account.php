<section>
<div class="page competition">

	<h1>Create Your Account</h1>

	<form name="createAccount" action="" method="post">

		<?php if(isset($msg)){ ?>
			<?php if ($msg === 'ERROR_INFORMATION'){ ?>
				<p class="msg-fail">Email and/or Password not valid</p>
			<?php } else if ($msg === 'ERROR_LOG_IN'){ ?>
				<p class="msg-fail">
					Account using this email already exist.
					Forget your Password? <a href="/forget-password?email=<?=$_POST['email'];?>">Get Help</a>.
				</p>
			<?php } else if ($msg === 'ERROR_REGISTER') { ?>
				<p class="msg-fail">Sorry, we couldn't register you. Please try later.</p>
			<?php } else if ($msg === 'ERROR_AGREE') { ?>
				<p class="msg-fail">You need to agree to the Privacy Policy and Terms of Use.</p>
			<?php } ?>
		<?php } ?>

		<label for="email">ENTER YOUR EMAIL ADDRESS</label>
		<input type="email" name="email" <?php if (isset($_POST['email'])){ echo 'value="'.$_POST['email'].'"'; }?>>

		<label for="newPassword">ENTER YOUR PASSWORD</label>
		<input name="password" <?php if (isset($_POST['password'])){ echo 'type="password" value="'.$_POST['password'].'"'; } else { echo 'type="text"'; } ?>>

		<label for="newsletter">
			<input type="checkbox" name="newsletter" <?php if (isset($_POST['newsletter'])){ echo 'checked'; } ?>>
			Sign up for our newsletter and get all the latest news &amp; updates
		</label>

		<label for="approval">
			<input type="checkbox" name="approval" <?php if (isset($_POST['approval'])){ echo 'checked'; } ?>>
			I agree that I have read and accepted the <a href="/privacy-policy">Privacy Policy</a>
			and <a href="/terms-and-conditions">Terms of Use</a>
		</label>


		<input type="submit" name="submit" class="button" value="Create an Account" >

	</form>

</div>
</section>
