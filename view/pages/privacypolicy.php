<section>
<div class="page">

	<h1>Privacy Policy</h1>
	<p>

		<h3>YOUR PRIVACY</h3>
		We are committed to maintaining the trust and confidence of visitors to our website.
		In particular, we want you to know that the NAC is not in the business of selling,
		renting or trading email lists with other companies and businesses for marketing
		purposes. In this Privacy Policy we provide detailed information on when and why we
		collect your personal information, how we use it, the limited conditions under which
		we may disclose it to others and how we keep it secure.<br>
		<br>

		<h3>COLLECTION AND USE OF PERSONAL INFORMATION</h3>
		Personal information means any information that may be used to identify you, such
		as, your name, title, phone number, email address, or mailing address.<br>
		<br>

		In general, you can browse NAC's website without giving us any personal
		information. We use several products to analyze traffic to this website in order to
		understand our customer's and visitor's needs and to continually improve our site for
		them. We collect only anonymous, aggregate statistics. For example, we do not tie a
		specific visit to a specific IP address.<br>
		<br>

		Additional activities on our site require you to create an account - such as the
		registration and submission pages, as well as the newsletter. As part of the
		registration process, we collect additional personal information - such as to identify
		you and your creative submissions, to update our database, for awarding prizes, etc.<br>
		<br>

		<h3>DISCLOSURE OF PERSONAL INFORMATION</h3>
		In some instances we rely on our channel partners to fulfill product trials and
		information requests, and provide participants with information about the NACs.
		To do this, we may pass your information to them for that purpose only, and they
		are prohibited from using that information for any other purpose. NAC sometimes
		hires vendor companies to provide limited services on our behalf. We provide
		those companies only the information they need to deliver the service, and they
		are prohibited from using that information for any other purpose. Because the NAC
		is a global business, your personal information may be shared with other NAC
		offices. The data protection laws in these countries may be more or less extensive
		than laws in the country in which you are located. However, NAC and its offices
		and subsidiaries are governed by this Privacy Policy and will use your personal
		information only as set forth in this policy.<br>
		<br>

		By using our website and providing us with your personal data, you consent to this
		transfer of your personal data.<br>
		<br>

		NAC may also disclose your personal information if required to do so by law or in
		the good faith belief that such action is necessary in connection with a sale, merger,
		transfer, exchange or other disposition (whether of assets, stock or otherwise) of all or
		a portion of a business of NAC or to (1) conform to legal requirements or comply with
		legal process served on this website; (2) protect and defend the rights or property
		of NAC and this website; (3) enforce its agreements with you, or (4) act in urgent
		circumstances to protect personal safety or the public.<br>
		<br>

		<h3>CHILDREN AND PRIVACY</h3>
		Our website does not offer information intended to attract children, nor does NAC
		knowingly solicit personal information from children under the age of 13.<br>
		<br>

		<h3>USE OF COOKIES</h3>
		A cookie is a small text file containing information that a website transfers to your
		computer's hard disk for record-keeping purposes and allows us to analyze our site
		traffic patterns. A cookie cannot give us access to your computer or to information
		beyond what you provide to us. Most web browsers automatically accept cookies;
		consult your browser's manual or online help if you want information on restricting or
		disabling the browser's handling of cookies. If you disable cookies, you can still view
		the publicly available information on our website. If are using a shared computer and
		you have cookies turned on, be sure to log off when you finish.<br>
		<br>

		<h3>ADWORDS</h3>
		AdWords Remarketing is a Remarketing and Behavioral Targeting service provided by Google.
		It connects the activity of www.nationaladvertisingchallenge.com with the Adwords advertising
		network and the Doubleclick Cookie.<br>
		<br>

		You can opt out of the cookie tracking here:
		<a href="http://www.google.com/settings/ads">http://www.google.com/settings/ads</a><br>
		<br>

		<h3>LINKS TO OTHER WEBSITES</h3>
		Our website contains links to information on other websites. On websites we do not
		control, we cannot be responsible for the protection and privacy of any information
		that you provide while visiting those sites. Those sites are not governed by this
		Privacy Policy, and if you have questions about how a site uses your information,
		consult that site's privacy statement.<br>
		<br>

		<h3>ACCESS TO YOUR PERSONAL INFORMATION</h3>
		You are entitled to access the personal information that we hold. Email your request
		to <a href="mailto:info@nationaladvertisingchallenge.com">info@nationaladvertisingchallenge.com</a>. An administrative fee may be payable.<br>
		<br>

		<h3>UPDATING YOUR PERSONAL INFORMATION AND UNSUBSCRIBING</h3>
		You may update your information or unsubscribe to general mailings at anytime by
		emailing <a href="mailto:info@nationaladvertisingchallenge.com">info@nationaladvertisingchallenge.com</a>.<br>
		<br>

		<h3>YOUR CONSENT</h3>
		By using our website, you consent to the collection and use of the information
		you provide to us as outlined in this Privacy Policy. We may change this Privacy
		Policy from time to time and without notice. If we change our Privacy Policy, we
		will publish those changes on this page. If you have any questions or concerns
		about our collection, use or disclosure of your personal information, please email
		<a href="mailto:info@nationaladvertisingchallenge.com">info@nationaladvertisingchallenge.com</a>.<br>
		<br>

		For more information about this site or your visit, please email us at
		<a href="mailto:info@nationaladvertisingchallenge.com">info@nationaladvertisingchallenge.com</a>.<br>
		<br>

		<em>This document was last updated on: 30/10/2012</em><br>

	</p>

</div>
</section>
