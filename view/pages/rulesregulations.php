<section>
<div class="page">

	<h1>Rules &amp; Regulations</h1>

	<p>
		<h3>BEFORE ENTERING</h3>
		Read through the category descriptions and submission requirements before you begin. Check the material
		requirements for each entry section; you will need to upload appropriate files and send physical material
		in order to be considered.
	</p>
	<p>
		<h3>CREATE YOUR NAC ACCOUNT</h3>
		There is no cost to register.<br />
		You will receive a confirmation email with your login and password information, as well as a unique ID number.
		You will need this number to enter and identify your entry.
	</p>
	<p>
		<h3>SUBMIT YOUR ENTRY</h3>
		Login using your user ID and password.<br />
		Begin the submission process by clicking on the "submit entry" button.<br />
		Select the category you would like to enter, and the number of submissions you would like to submit
		(you can enter as many categories and submit as many pieces as you like).<br />
		Complete the entry form for each submission you are entering.<br />
		Upload your entry media/digital files, making sure they conform to the requirements.<br />
		Incomplete entries can be saved at any stage. You must pay the submission fee for the entry to be submitted
		successfully. Entries will not be judged unless submission process is finalized and payment is received.
	</p>
	<p>
		<h3>PAYMENT</h3>
		Payment must be made online by VISA, MasterCard or AMEX.<br />
		Retain the receipt for your records. You will receive an email confirmation that the payment was successful
		and that your entry has been successfully submitted and accepted. Only entries that have been paid for will
		be submitted for judging.
	</p>
	<p>
		<h3>MATERIAL</h3>
		Please check the material requirements before submitting anything. Make sure all material adheres to
		specifications and sizes. All materials must be received on or before the closing date and time. Late
		materials will not be accepted. Once you pay the required submission fee, you
		will be provided with a label "template" that can be adhered to your materials. We recommend you use
		this label as it will streamline the process.
	</p>
	<p>
		<h3>ELIGIBILITY</h3>
		Entries must be submitted by deadline.<br />
		Teams: two or more (however, only 2 will receive the prize), teams of 1 for the student category<br />
		Only work created solely for the purpose of entering this competition is eligible.<br />
		Entrants must use royalty-free or licensed music for all submissions (including presentation videos).<br />
		The National Advertising Challenge is open to all those involved in advertising and communication - advertising
		agencies, production companies, advertisers, etc. All individuals involved in the creative process are eligible
		to enter. The student category is open to all those involved in a creative post&ndash;secondary program across
		Canada.<br />
		Teams (or team members) may submit to more than one category.
	</p>
	<p>
		<h3>FEES &amp; SUBMISSIONS</h3>
		Submissions are $<?=$priceFull;?>. Each additional submission is $<?=$priceDiscount;?>.<br />
		Student submissions are free.<br />
		Your payment must be submitted online with your entry. Entries cannot be processed until payment is received.
		Be sure to review your entry before finalizing your submission. There are no refunds. You can pay by VISA,
		MasterCard, or AMEX. We use Stripe to handle payments.
	</p>
	<p>
		<h3>ABOUT THE PRIZE</h3>
		All categories (except the Student Category) are open to all ages. If you are a Young Lion (born after June 21, 1985)
		you will win accommodations along with your prize!<br><br>
		CATEGORIES (All except Student Category): Gold category winners receive round trip airfare* from Toronto to Nice and
		paid 4-day registration to the 2016 Cannes Lions Advertising Festival. (Max 2 winners per team).<br><br>
		YOUNG LIONS (entrants born after June 21 1985): Gold category winners receive round trip airfare* from Toronto to Nice,
		a 4 -day pass to the 2016 Cannes Lions Advertising Festival AND paid accommodations** for a 4-night stay (based on double
		occupancy). (Max 2 winners per team).<br><br>
		STUDENT CATEGORY (enrolled in any creative program across Canada): The team of winners of the student category will
		receive a 2 month internship at BBDO Toronto.
	</p>

	<p class="small-p">
		* Those wishing to change their travel plans will be reimbursed $1,230.00 CAD to book their own travel arrangements.
		Any additional costs will be at the winners expense<br />
		** Accommodation is chosen by The National Advertising Challenge and Cannes Lions Advertising Festival.<br />
		*** 13" MacBook Pro with retina display will be awarded to Gold category winners who do not wish to travel to Cannes.<br />
		Please contact ellie@nationaladvertisingchallenge.com with any questions.<br />
		<br />
		This document was last updated on: 05/06/2015
	</p>

</div>
</section>
