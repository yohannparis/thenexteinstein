<section>
<div class="page entry-content">

	<h1>Charting new territory for your brand.</h1>
	<p>
		Who knows what lies ahead for your brand when Canada’s most talented creatives get a
		hold of it? They have the ability to bring a fresh new perspective and previously
		overlooked potential. And at a fraction of the cost of a full creative review.
		But only if you&nbsp;participate.
	</p><p>
		Download the brochure and find out how little you’ll have to pay to have some of the
		brightest minds in the advertising industry focus on your brand. Did we mention you
		get to write the brief and own all the&nbsp;work?
	</p><p>
		Read more about our differents sponsorships:
		<a href="/media-sponsorships">Media</a>,
		<a href="/brand-sponsorships">Brand</a>, and
		<a href="/presenting-sponsorships">Presenting</a> sponsorships.
	</p><p>
		To see what the NAC has done for past sponsors, <a href="/winners2015">click here</a>.
	</p>

	<a class="downloadButton" href="/model/2016/pdf/sponsorship_package-May-2016.pdf" target="_blank">
		Download the 2016<br />Sponsorship Package
	</a>

</div>
</section>
