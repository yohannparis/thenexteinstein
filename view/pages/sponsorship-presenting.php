<section>
<div class="page">

	<h1>Charting new territory for your brand.</h1>
	<h2>Presenting Sponsor</h2>
	<p>
		Contribution: $50,000  <br>
		An opportunity for dominant presence from the Creative Brief to the Cannes Festival as the
		presenting and entertainment sponsor of the National Advertising Challenge.  
	</p>
	<ul>
		<li>Preeminent logo positioning: National Advertising Challenge sponsored by, ‘your brand’ </li>
		<li>Recognized as a leader promoting creativity and innovation in communication </li>
		<li>Branded messages to 8,000 Creatives and Media Planners </li>
		<li>Frequent posts to social media</li>
		<li>Host Creative or Media Judging </li>
		<li>Judge on one of the Juries </li>
		<li>Double Page Spread in the Gala magazine </li>
		<li>10 tickets to the Gala </li>
		<li>Swag bag identification  </li>
	</ul>
	<p>
		<a class="big-link" href="/model/2016/pdf/sponsorship_package-May-2016.pdf" target="_blank">
			Download the brochure</a> and find out how little you’ll have to pay to have hundreds of
			the country's top advertising agencies focus on your brand.
	</p>

</div>
</section>
