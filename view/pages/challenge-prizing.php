<section>
	<div class="page">

		<h1>Navigating your newfound stardom.</h1>
		<h2>2016 Prizing</h2>

		<p>
			Based on your age, your status and how good you are, determines how you should pack. So pay attention.
		</p><p>
			<h3>GOLD WINNERS</h3>
			Flight to Cannes + 4-Day Registration to Cannes Festival.
		</p><p>
			<h3>UNDER 28 GOLD WINNERS</h3>
			Flight to Cannes + Accommodations + 4-Day Registration to Cannes Festival. 
		</p><p>
			<h3>AT HOME GOLD WINNERS</h3>
			Category winners who prefer not to travel to Cannes will receive a 13-inch MacBook Pro with retina display. 
		</p><p>
			<h3>STUDENT WINNERS</h3>
			2-Month Internship at an advertising agency. 
		</p><p class="small-p">
			NAC only pays for flight, registration and hotel for under 28’s for 4 nights. Additional expenses
			not included. Accommodations are chosen by NAC (don’t worry it won’t be a Motel 6). Prizes cannot
			be deferred. Prefer to make your own travel plans? NAC will reimburse you the cost of the flight
			and the registration (as well as the hotel for under 28’s)!.
		</p>

		<p><a class="big-link" href="rules-and-regulations">Click here for full rules and regulations.</a></p>

	</div>
</section>
