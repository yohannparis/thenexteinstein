<section>
<div class="page">

	<article>

		<div>
			<h1>No guts, No Glory</h1>
			<p>
				The gauntlet has been thrown down. The question is, do you have the stones to pick it up?
				It's time to put your money where your mouth is. No layers of approval, no account people
				holding you back, it's just you and your creativity that stand between a potential salary
				raise, exposure among the industry's finest and an unforgettable trip to the world's most
				prestigious creative festival in Cannes, France. Check out the briefs. Take a deep breath
				and enter now.
			</p>
			<p>Go ahead, we dare you.</p>
		</div>

		<div>
			<h2>Mark Your Calendars</h2>
			<p>
				<strong>April 7 2014</strong> Competition Ends<br>
				<strong>April 2014</strong> Media and Creative Judging<br>
				<strong>May 1st 2014</strong> National Advertising Awards Gala
			</p>
		</div>

	</article>

	<article class="categories">

		<h2>Categories</h2>
		<?php
			foreach($categories as $key => $category){
				$price = $priceFull.'<sup>*</sup>';
				if($key == 'student'){ $price = $priceStudent; }
				$briefPDFlink = '/brief/2014/'.$key;
				$briefText = 'Download the Brief';
		?>
			<div class="category">
				<img src="/model/2014/sponsors_logos/<?=$category['sponsor-logo'];?>" alt="<?=$category['title'];?>" width="150">
				<h3><?=$category['title'];?></h3>
				<p><?=$category['text'];?></p>
				<span class="price">$<?=$price?></span>
				<a href="<?=$briefPDFlink;?>" class="button"><?=$briefText;?></a>
			</div>
		<?php }?>

		<p>* Cost of first entry. Each subsequent entry is $<?=$priceDiscount;?>.</p>

	</article>


</div>
</section>
