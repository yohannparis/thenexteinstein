<section>
	<div class="page">

		<h1>You are here.<br>Here's how you get there.</h1>
		<h2>About the challenge</h2>
		<p>
			There’s plenty of ways to get to Cannes. Planes, boats, hallucinates.
			But the NAC is by far the cheapest and most fun. Pay the entry fee,
			download the brief for your preferred category and submit your ideas.
			If you win, you get an all-expense paid trip to Cannes. Which is way
			more fun than a peyote-induced mind-trip to France... for the most&nbsp;part. 
		</p><p>
			<a class="big-link" href="/categories">Browse 2016 categories</a>
			to see how both can get&nbsp;there.
		</p>

	</div>
</section>
