<section>

	<div class="slideshow">

		<ul class="slides">
			<li id="slide-0" class="slide">
				<img src="/view/images/Home_Main_short.jpg" alt="1">
				<span>
					<strong>Einstein changed the world with one idea. Now it’s your&nbsp;turn.</strong>
				</span>
			</li>
			<?php /*
			<li id="slide-1" class="slide">
				<img src="/model/2015/slides/NAA_2015_Website_HomepageImages_v1d-02.jpg" alt="2">
				<span>
					<strong>The Agency Competition</strong>
					200 agencies compete, who will be crowned The Most Creative Agency of the Year?
				</span>
			</li>
			<li id="slide-2" class="slide">
				<img src="/model/2015/slides/NAA_2015_Website_HomepageImages_v1d-04.jpg" alt="4">
				<span>
					<strong>
						Our team of media buyers and strategists will help you bring the twenty five plus creative concepts to life
					</strong>
				</span>
			</li>
			<li id="slide-3" class="slide">
				<img src="/model/2015/slides/NAA_2015_Website_HomepageImages_v1d-05.jpg" alt="5">
				<span>
					<strong>The National Advertising Challenge Advisory Board</strong>
				</span>
			</li>
			*/ ?>
		</ul>

		<a href="javascript:void(0)" class="arrow prev">Previous</a>
		<a href="javascript:void(0)" class="arrow next">Next</a>

		<script type="text/javascript">

			// Get the list and all the slides
			var slides = document.getElementsByClassName('slides')[0];
			var slide = document.getElementsByClassName('slide');

			// Add the style for the numbers of slides
			slides.className += ' slides-' + slide.length;

			// Clone the last two slides and add it at the beggining for smooth transition
			slides.insertBefore(slide[slide.length-1], slide[0]);
			slides.insertBefore(slide[slide.length-1], slide[0]);

			// Add a marker to the 'first' element of the slide, now in the middle
			slide[((slide.length-1)/2).toFixed(0)].className += ' active';

			// Function to move elements
			function move(direction) {
				var slides = document.getElementsByClassName('slides')[0];
				var slide = document.getElementsByClassName('slide'); // Get the list of artworks

				switch (direction){ // Select what direction we are going
					case 'prev': // If we are going backward
						slides.insertBefore(slide[slide.length-1], slide[0]); // Move the last element of the list at the front
						break;
					case 'next': // If we are moving forward
					default:
						slides.appendChild(slide[0]); // move the first element of the list to the end
						break;
				}

				// Update the marker
				document.getElementsByClassName('active')[0].className = 'slide';
				slide[((slide.length-1)/2).toFixed(0)].className += ' active';
			}

			// On the click of one of the arrow
			document.getElementsByClassName('arrow prev')[0].addEventListener('click', function() { move('prev');	});
			document.getElementsByClassName('arrow next')[0].addEventListener('click', function() { move('next');	});

			// On the press of one of the keyboard arrow
			document.onkeydown = function(e) {
				e = e || window.event;
				switch (e.keyCode) {
					case 37: move('prev'); break; // Left key
					case 39: move('next'); break; // Right key
				}
			};

		</script>

	</div>

	<div class="wrapper">

		<div class="page-index">
			<h1>The next Einstein is a competition with an audacious&nbsp;goal:</h1>
			<p>To discover and celebrate ideas that could change the&nbsp;world.</p>
		</div>

		<?php /*
		<div class="index-left counter">
			<?php
				$deadline = 'October 30, 2015 23:59:59';
				$remaining = strtotime($deadline) - time();
				$days  = floor( $remaining / 86400 );
				$hours = floor( ($remaining - ($days * 86400) ) / 3600 );
				$mins  = floor( ($remaining - ($days * 86400) - ($hours * 3600) ) / 60 );
				$secs  = floor( ($remaining - ($days * 86400) - ($hours * 3600) - ($mins * 60) ) / 1 );
			?>
			<script type="text/javascript">

				function twoDigits(n){ return (n <= 9 ? "0" + n : n); }

				function calculate(secDiff, unitSeconds){
					var tmp = Math.abs((tmp = secDiff/unitSeconds)) < 1? 0 : tmp;
					return Math.abs(tmp < 0 ? Math.ceil(tmp) : Math.floor(tmp));
				}

				function updateTimer(){
					var secDiff = Math.abs(Math.round((Date.now()-Date.parse('<?=$deadline;?>'))/1000));
					var counter = document.getElementsByClassName('counter')[0].getElementsByTagName('span');

					var days 	= calculate(secDiff,86400);
					counter[0].innerHTML = twoDigits(days) + counter[0].innerHTML.substr(2);

					var hours = calculate((secDiff-(days*86400)),3600);
					counter[1].innerHTML = twoDigits(hours) + counter[1].innerHTML.substr(2);

					var mins 	= calculate((secDiff-(days*86400)-(hours*3600)),60);
					counter[2].innerHTML = twoDigits(mins) + counter[2].innerHTML.substr(2);

					var secs 	= calculate((secDiff-(days*86400)-(hours*3600)-(mins*60)),1);
					counter[3].innerHTML = twoDigits(secs) + counter[3].innerHTML.substr(2);
				}

				setInterval(updateTimer, 1000);
			</script>

			<h3>Submissions close in:</h3>
			<span><?=sprintf('%02d',$days);?><sub>days</sub></span><em>:</em>
			<span><?=sprintf('%02d',$hours);?><sub>hours</sub></span><em>:</em>
			<span><?=sprintf('%02d',$mins);?><sub>mins</sub></span><em>:</em>
			<span><?=sprintf('%02d',$secs);?><sub>secs</sub></span>
		</div>
		*/ ?>

		<?php /*
		<a class="index-left downloadButton" href="/model/2016/pdf/sponsorship_package-May-2016.pdf" target="_blank">
			Download the 2016<br />Sponsorship Package</a>
		<br><br>
		<a class="index-left downloadButton" href="/briefs">Check out the 2015 categories and sponsors</a>
		*/ ?>

  </div>

</section>
