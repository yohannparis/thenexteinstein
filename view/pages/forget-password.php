<section>
<div class="page competition">

	<h1>Forgot password</h1>

	<?php
		switch ($state) {
		/*
			Change password states:
				1 - form to enter email address. -> GET, error POST
				2 - Check if email correct and used by an user, then send the verification email. -> POST
				3 - User acces page from link from email, form to enter new password. -> GET, error POST
				4 - Password changed, -> POST
				5 - Password NOT changed, -> POST
		 */
				case 5: // 5 - Password NOT changed. ?>

			<p><span class="msg-fail">Sorry, but your password can not be changed!</span></p>
			<p>
				Have you already activated your account?<br>
				If not, check your email to complete your registration.<br>
				Or, please <a href="/contact">contact us</a>, or try again later.
			</p>

		<?php	break; ?>

		<?php case 4: // 4 - Password changed. ?>

			<p>
				<span class="msg-success"><strong>Thank you</strong>, your password has been changed!</span><br>
				You can now login with your new password.
			</p>
		<?php	break; ?>

		<?php case 3: // 3 - User acces page from link from email, form to enter new password. ?>

			<form name="changePassword" action="/forget-password" method="post">

				<input type="hidden" name="email" value="<? if (isset($passwordValidMsg)) { echo $_POST['email']; }else{ echo $email; } ?>" />
				<input type="hidden" name="verifNumber" value="<? if (isset($passwordValidMsg)) { echo $_POST['verifNumber']; }else{ echo $verifNumber; } ?>" />

				<label for="newPassword">NEW PASSWORD
				<?php if(isset($passwordValidMsg['newPassword'])){ $msg1 = 'msg-fail'; ?>
					<span class="msg-fail"><? echo $passwordValidMsg['newPassword']; ?></span>
				<? } ?>
				</label>
				<input class="<?=$msg1;?>" name="newPassword" type="password"
							 value="<?php if (isset($passwordValidMsg)) { echo $_POST['newPassword']; } ?>">


				<label for="newPassword2">CONFIRM PASSWORD
				<?php if(isset($passwordValidMsg['newPassword2'])){ $msg2 = 'msg-fail'; ?>
					<span class="msg-fail"><? echo $passwordValidMsg['newPassword2']; ?></span>
				<? } ?>
				</label>
				<input class="<?=$msg2;?>" name="newPassword2" type="password">

				<input type="submit" class="button" value="Change Password" />

			</form>

		<?php	break; ?>

		<?php case 2: // 2 - Check if email correct and used by an user, then send the verification email. ?>

			<p>
				You will receive a verification email - follow the instructions to complete your request.<br>
				Having trouble? <a href="/contact">Contact Us</a>.
			</p>

		<?php	break; ?>

		<?php default: // 1 - form to enter email address. ?>

			<p>
				To reset your password, enter your email below.<br>
				You will receive a verification email - follow the instructions to complete your request.<br>
				Having trouble? <a href="/contact">Contact Us</a>.
			</p>

			<form name="changePassword" action="" method="post">
				<input type="hidden" name="sendEmail" value="yes" >
				<label for="email">ENTER YOUR EMAIL ADDRESS
					<?php $msg=''; if(isset($emailValidMsg['email'])){ $msg = 'msg-fail'; ?>
						<span class="msg-fail"><?php echo $emailValidMsg['email']; ?></span>
					<?php } ?>
				</label>
				<input class="<?=$msg;?>" name="email" type="email" value="<?php if(isset($_REQUEST['email'])){ echo htmlentities($_REQUEST['email'], ENT_QUOTES, 'UTF-8'); } ?>">
				<input type="submit" class="button" value="Send Verification Email" >
			</form>

			<p>Not Registered? <a href="/create-account">Sign up.</a></p>

		<?php	break; ?>

	<?php } // end switch ?>

</div>
</section>
