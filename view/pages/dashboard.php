<section>
<div class="page page-dashboard">

	<h1><?=$_SESSION['email'];?></h1>

	<article class="submission">

		<h2>Enter now!</h2>
		<p>
			Click the button below. A new entry box will appear on the right with your unique entry code and a link to
			begin your submission process. Please be sure to input your unique entry code into all the required areas
			of your entry and ensure that you have satisfied all submission requirements before uploading your files.
			<a href="/entry-specifications">Click here to review the submission&nbsp;requirements.</a>
		</p>

		<?php if(date('Ymd') < '20150331'){ // before the deadline on March 30. ?>
			<a class="button" href="/dashboard/create">New submission</a>
		<?php } ?>

		<?php if(isset($step0) and !$step0){ ?>
			<p class="msg-fail">
				Sorry, we couldn't create you a new submission!
				Please try later or <a href="/contact">contact&nbsp;us</a>.
			</p>
		<?php } ?>

	</article>

	<article class="entries">

		<?php
			// ---- Get all the entries done or in progress by the user
			$entries = userEntries($DBlink, $_SESSION['id']);
			foreach($entries as $entry){
				$category = $categories[$entry['category']];
		?>
			<div class="entry step<?=$entry['stepProcess'];?>">
				<p class="entry-code"><span>Unique Entry Code</span><strong><?=$entry['entryCode'];?></strong></p>
				<p class="entry-category">
					<span>Category</span>
					<?php
						if(is_null($entry['category'])) { echo '-'; }
						else { echo $category['title']; }
					?>
				</p>
				<p class="entry-date"><span>Submission Started</span><?=date('F d, Y', strtotime($entry['date']));?></p>
				<p class="entry-modified">
					<span>Last Modified</span>
					<?php
						if(is_null($entry['modified'])) { echo '-'; }
						else { echo date('F d, Y', strtotime($entry['modified'])); }
					?>
				</p>

				<?php if(true or date('YmdH') < '2015033106'){ // before the deadline on March 31 at 6am. ?>

					<?php 			 if($entry['stepProcess'] == 1){ ?><a class="button" href="/submit/step1/<?=$entry['entryCode'];?>">Start</a>
					<?php } else if($entry['stepProcess'] < 4 ){ ?><a class="button" href="/submit/step<?=$entry['stepProcess'];?>/<?=$entry['entryCode'];?>">Continue</a>
					<?php } else if($entry['stepProcess'] == 4){ ?><a class="button" href="/submit/step4/<?=$entry['entryCode'];?>">Review, then Pay</a>
					<?php } else if($entry['stepProcess'] == 5){ ?><a class="button" href="/submit/step5/<?=$entry['entryCode'];?>">Pay to complete</a>
					<?php } else if($entry['stepProcess'] == 6){ ?><a class="button">Submitted</a><?php } ?>

				<?php } else { ?>

					<?php if($entry['stepProcess'] == 6){ ?><a class="button">Submitted</a>
					<?php } else { ?><strong>Submission Closed</strong><?php } ?>

				<?php } ?>
			</div>
		<?php } ?>

	</article>

	<article>

		<h2>Rules Were Meant To Be Broken. (Except For These Ones.)</h2>
		<ol>
			<li>Entries must be submitted from March 2, 2015 to March 30, 2015.
			<li>
				You can have as many team members as you want but only two will receive a prize.
				(Although we appreciate your moxy for attempting to get your whole department to France)
			<li>Submissions may only use royalty-free music including presentation videos.
			<li>NAC is open to all companies involved in advertising and communication. Ad agencies, production companies, advertisers, etc.
			<li>Team members may submit to more than one category.
			<li>
				Payment must be submitted online with your entry. Entries cannot be processed until payment is received.
				Please review your submission before sending as there are no refunds.
			<li>Payment methods include Visa, MasterCard, or Amex. Cheques or cash will not be accepted. Nor your first born.
			<li>
				The Student Category is open to students from any creative post secondary program across Canada.
				Students must submit proof of enrollement to be eligible for the 4 months internship at
				<a href="http://www.bbdo.ca/">BBDO Toronto</a>, courtesy of BBDO and Royal Bank of Canada.
				Students enter as teams of 2 and will both win internships.
		</ol>

	</article>

</div>
</section>
