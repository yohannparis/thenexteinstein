<section>
<div class="page">

	<h1>Charting new territory for your brand.</h1>
	<h2>Media Partnerships</h2>
	<p>
		Who knows what lies ahead for your brand when Canada’s most talented creatives
		get a hold of it? They have the ability to bring a fresh new perspective and
		previously overlooked potential. But only if you participate.
	</p>
	<p>
		Contribution: $10,000 <br>
		Our Media partners sponsor the winners’ trip to Cannes for their respective medium.
		Sponsorship in each media category is exclusive. Not only is your name part of a robust
		campaign to 8,000 creatives and media planners throughout the country you’ll be part of
		the final Gala celebration where 600 Creatives and Media Planners from around the country
		gather to see the winners of the National Advertising Challenge. But mostly it's about
		connections; when was the last time you were able to reach out and touch the Creatives
		and Media Planners who produced the work that runs on your medium?  
	</p>
	<p>
		This is your opportunity to meet and engage with those you have not met at the best party
		our industry holds without trying to spill your drink on the competition, so all you have
		to worry about, is what you are going to wear.  
	</p>
	<ul>
		<li>Logo exposure to 8,000 Creative and Media Planners </li>
		<li>Recognized as a leader promoting creativity in advertising and media planning </li>
		<li>Frequent posts to social media </li>
		<li>Full page ad in Gala magazine </li>
		<li>4 tickets to the Gala </li>
		<li>Promotional item in swag bag </li>
		<li>Additional opportunity to host the Media Juding</li>
	</ul>
	<p>
		<a class="big-link" href="/model/2016/pdf/sponsorship_package-May-2016.pdf" target="_blank">
			Download the brochure</a> and find out how little you’ll have to pay to have hundreds of
			the country's top advertising agencies focus on your brand.
	</p>

</div>
</section>
