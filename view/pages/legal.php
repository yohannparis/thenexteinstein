<section>
<div class="page">

	<h1>Legal</h1>
	<p>

		<h3>GENERAL</h3>
		The National Advertising Challenge offer creatives and media planners from the world of
		advertising an opportunity to promote themselves by showing their best and most
		original work.<br>
		<br>

		NAC Organizers may refuse Entries, which offend national or religious sentiments,
		public taste or which in the Awards Organizer's opinion breach any applicable laws,
		regulations/codes of practise or infringe any third party rights. Entries cannot be
		cancelled or removed from the competition after April 2013 (please check back for
		specific date).<br>
		<br>

		NAC Organizers may contact the client related to any entry at the request of the jury
		at any time during the voting process should any questions about the implementation
		or presentation of the work arise. The Festival Organizers will endeavour to move
		entries to more appropriate categories if necessary. However, the Jury will not be
		allowed to move entries between categories. All entry forms must be completed
		online at <a href="https://nationaladvertisingchallenge.com">nationaladvertisingchallenge.com</a>.
		An entry sent online will be considered
		complete once all corresponding fees have been paid and material arrives at NAC
		offices and is uploaded via the entry website.<br>
		<br>

		<h3>ENFORCEMENT OF THE RULES</h3>
		Entrants or companies who are proved to have deliberately and knowingly
		contravened any rules relating to eligibility may be barred from entering the awards
		for a period of time following the NACs as specified by Organizers.<br>
		<br>

		<h3>JUDGING</h3>
		Entries will be judged by a National Jury. The shortlist is decided by the first voting.
		Further voting establishes the ranking in each category which is the basis for the
		Juries' discussions and awarding of Gold, Silver, Bronze and Merit awards.<br>
		<br>

		At all voting stages, a judge is prevented from voting for any entry submitted by his/
		her agency(ies) in his/her province. The decision of the Juries in all matters relating to the
		awarding of prizes will be final and binding.<br>
		<br>

		<h3>TREATMENT AND PUBLICATION OF ENTRIES</h3>
		For the purpose of this paragraph, 'entry/campaign' means the work you enter
		into the NACs under these Entry Rules. Any material submitted in the course of
		entering the Awards or otherwise provided to the Organizers becomes the property of
		Organizers and cannot be returned.<br>
		<br>

		Entrants may be required to supply additional material for any shortlisted or winning
		work for the winners' DVD and any promotional publication and exhibitions held after
		the Awards Gala.<br>
		<br>

		In order to promote the NACs, each entrant hereby grants to the Festival Organizers
		a non-exclusive, royalty free right and licence to:<br>
		<br>

		Screen or publish all materials submitted, including all entry/campaign, with or without
		charge at public or private presentations, wherever and as often as Organizers think
		fit;<br>
		<br>

		Reproduce all materials submitted to Organizers, including all entry campaign, in the
		NAC Archive, online or offline.<br>
		<br>

		You warrant that, in exercising the rights and licences granted to NAC Organizer, you
		shall not breach any applicable laws or infringe the rights of any third party, including
		but not limited to third party intellectual property rights, anywhere in the world.<br>
		<br>

		Each entrant agrees to assist the Organizers in supporting any legal action that may
		be taken against the Organizer in relation to the exercise of the rights set out above
		and to supply information to Organizers immediately should they become aware that
		an unauthorized collection or compilation including their Advertisement is available for
		sale or distribution.<br>
		<br>

		Each entrant agrees to hold the Organizers harmless of any claims that may be made
		against them by reason of any breach of the warranty set out above. Winners have
		the right to use any award given to them for promotional purposes on condition that
		this is correctly described.<br>
		<br>

		Each entrant confirms to the Organizers that they have the legal right to enter the
		NACs on the terms of these Entry Rules. Each entrant indemnifies Organizers against
		all liability to any other person, firm or company and all loss arising from a breach by
		the entrant of any of these Entry Rules.<br>
		<br>

		<h3>MISCELLANEOUS</h3>
		The National Advertising Challenge (the "Challenge") are organized and managed by The Metrick System,
		a company registered in Canada and the U.S. References to "us", "we", and "our" shall be construed
		accordingly. Each entrant accepts full responsibility for the quality of entries and discharges
		the Organizers from any responsibility in respect of third parties.<br>
		<br>

		All entrants will strictly observe the Entry Rules. Completion and signature sending
		of the Entries Payment Form will imply full acceptance by each entrant of the
		Entry Rules. Non-compliance with any of the Entry Rules will result in automatic
		disqualification of the entry.<br>
		<br>

		The decisions of the Organizers in all matters relating to the Festival shall be final and
		binding.<br>
		<br>

		In the event of a win, any duties, fees and charges accrued from the transporting of
		the trophy, will be covered by the recipient, not the Organizers.<br>
		<br>

		These Entry Rules shall be governed by and construed according to Canadian law
		and the parties submit to the exclusive jurisdiction of the Canadian courts.<br>
		<br>

		Neither The Metrick System, trading as National Advertising
		Challenge, shall be deemed to be in breach of this Agreement or otherwise liable to
		the Client for any failure or delay in performing its obligations under this Agreement
		as a result of an event or series of connected events outside our reasonable
		control(including, without limitation, acts of God, floods, lightning, storm, fire,
		explosion, natural disaster (including, without limitation, ash cloud), war, military
		operations, acts of terrorism or threats of any such acts, any strike action, lock-outs
		or other industrial action, or governmental or regulatory order (including prohibitions
		on public gatherings) and a pandemic, epidemic (such as swine flu or other disease)
		or other widespread illness, individually or collectively being an "Event of Force
		Majeure").<br>
		<br>

		<em>This document was last updated on: 29/09/2014</em>

	</p>

</div>
</section>
