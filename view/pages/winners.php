<section class="old-winners">
	<div class="page">

		<article>
			<h2>2014 Winner</h2>
			<h3>Grand Prize Winner</h3>
			<img src="/model/waiting-page/2014.jpg" alt="2014 Winner" >
			<h4>
				New Approach to Prosthetic Design and Integration<br>
				Medical biotech, technology<br>
				Marin Schultz, Canada
			</h4>
			<p>
				"I want to change the world by making strong, inexpensive, 3D printed prosthetic hands. These hands would be
				controlled with a new breath pressure system and be easily serviceable with commonly available parts. It is my
				dream to help people. "
			</p><p>
				Patients who are living with the loss of their hands are unable to take care of themselves without some form of
				prosthetic device. Outside of North America, 80 to 85 percent of worldwide amputees are survivors of blasts from
				land mines. Mines are responsible for over 26,000 amputations per year and have produced 300,000 amputees worldwide.
				They have caused more injury than both nuclear devices exploded in Hiroshima and Nagasaki combined (Berry, Dale.
				From Land Mines to Lawn Mowers Prosthetic Rehabilitation Proceeds One foot at a Time, The Washington Diplomat, 19
				January 2005). My dream is to help these people. My proposal takes advantage of new inexpensive 3D printing technology
				to create a robust 3D printed arm and hand. I have already developed a prototype arm/hand and I have invented a new
				prototype breath-pressure control system. This prosthetic is both easy to control, robust and inexpensive.
			</p><p>
				The plans and technology I develop for this project would be made available online or in kit-form. The electronic parts
				are easily obtained at any electronics hobby store and could potentially be scavenged from existing technology. It is
				my goal to establish local, sponsored prosthetic dispensary nodes around the world, which would custom print, assemble
				and fit the devices to any and all who need them. My system is easily repaired as all wearing parts are re-printable,
				and my control system just as reliable as traditional control systems (EMG and muscle flexion) but at a fraction of the
				cost. I estimate that one hand/arm combination with a controller could be produced with wholesale parts for around $300
				Canadian dollars. This makes it among the least expensive high functioning prosthetics available in the world.
			</p>

		</article>
		<article>
			<h2>2013 Winner</h2>
			<h3>Grand Prize Winner</h3>
			<img src="/model/waiting-page/2013.jpg" alt="2013 Winner" >
			<h4>
				Genetically modifying dragonflies to reduce harmful effects of the carbon dioxide buildup in the atmosphere.<br>
				Charles Rose, Canada
			</h4>
			<p><strong>BIG IDEA to improve the world</strong></p>
			<p>
				Use molecular biotechnology techniques to develop photosynthetic dragonflies. Genetically engineered dragonflies carry
				not only chloroplasts but also other necessary regulatory genes. These photosynthetic dragonflies are expected to reproduce,
				generating photosynthetic offspring (i.e., it is a self-perpetuating process). A mathematical model based on differential
				equations can show the initial mass of viable dragonflies that is necessary in order to have a sustainable and non-negligible
				photosynthetic effect and, thus, an impact on the environment (i.e., carbon dioxide uptake). The expectation is that implementing
				a biotechnology program that can produce photosynthetic dragonflies continually will have a substantial effect on global warming
				by capturing carbon dioxide molecules. Since dragonflies do not constitute a threat to humans, farm animals, or agriculture, they
				are considered a safe choice.
			</p>

		</article>
	</div>
</section>
