<?php
	$section = 'sponsorship';
	$description = 'Request for Brand Sponsorship Brochure';
	include '_view/header.php';
?>
<section>

	<div class="page">

		<h1 class="header">Request for 2015 Brand Sponsorship Brochure</h1>

		<!-- SharpSpring Form for Download brochure -->
		<script type="text/javascript">
			var ss_form = {'account': 'MzIwNwEA', 'formID': 'S0lOTDNJM7LUTUo1TtE1SUkx1bVIM7bUNTBKS0lKMk-xNE80BAA'};
			ss_form.width = '100%';
			ss_form.height = '1000';
			ss_form.domain = 'app-16DGM2G.sharpspring.com';

			// Removing the forced style by the sharpsring iframe
			setTimeout(function(){
				document.getElementsByTagName('section')[0].setAttribute('style', '');
			}, 1000);
		</script>
		<script type="text/javascript" src="https://koi-16DGM2G.sharpspring.com/client/form.js?ver=1.1.1"></script>
	</div>

</section>
<?php include '_view/footer.php'; ?>
