<section>
<div class="page">

	<h1 class="header">Request for 2016 Brand Sponsorship Brochure</h1>

	<?php
		error_reporting(0);

		$send = false;

		if (isset($_POST['name']) && isset($_POST['email'])){
			if ($_POST['email'] != ''){
				$to = "ellie@nationaladvertisingchallenge.com";
				$headers =  "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/plain; charset=UTF-8\r\n";
				$headers .= "From: ".$_POST['name']." <".$_POST['email'].">\r\n";
				$body =  "Name: ".$_POST['name']."\n";
				$body .= "Email: ".$_POST['email']."\n";
				$body .= "Phone: ".$_POST['phone']."\n";
				$body .= "Company: ".$_POST['company']."\n";
				$body .= "Title: ".$_POST['title']."\n";
				$body .= "\nBrand Name for Sponsorship?:\n\n".$_POST['brand'];
				$send = mail($to, "2016 NAC - Sponsorship Request", $body, $headers);
			}
		}

		if ($send) {
	?>
		<p>Thank you! You’re message has been sent, We’ll be in touch within 48 hours.</p>

	<?php } else { ?>

		<form method="post" action="" novalidate="novalidate">

			<ul>
				<li>
					<label>Name</label><input type="text" name="name">
				</li><li>
					<label>Company</label><input type="text" name="company">
				</li><li>
					<label>Title</label><input type="text" name="title">
				</li><li>
					<label>Phone</label><input type="text" name="phone">
				</li><li>
					<label>Email</label><input type="text" name="email">
				</li><li>
					<label>Brand Name for Sponsorship?</label>
					<textarea name="brand" cols="40" rows="10"></textarea>
				</li>
			</ul>

			<button type="submit">Submit</button>
		</form>

	<?php } ?>

</div>
</section>
