<section>
<div class="page">

	<h1>TERMS &amp; CONDITIONS</h1>
	<p>

		<h3>INTRODUCTION</h3>
		This disclaimer governs your use of our website; by using our website, you accept
		this disclaimer in full. If you disagree with any part of this disclaimer, you must not use
		our website.<br>
		<br>

		<h3>INTELLECTUAL PROPERTY RIGHTS</h3>
		Unless otherwise stated, we or our licensors own the intellectual property rights
		in the website and material on the website. Subject to the license below, all these
		intellectual property rights are reserved.<br>
		<br>

		<h3>LICENSE TO USE WEBSITE</h3>
		You may view, download for caching purposes only, and print pages from the website
		for your own personal use, subject to the restrictions below.<br>
		<br>

		<h3>You must not:</h3>
		Republish material from this website (including republication on another website);
		sell, rent or otherwise sub-license material from the website; show any material from
		the website in public; reproduce, duplicate, copy or otherwise exploit material on
		our website for a commercial purpose; edit or otherwise modify any material on the
		website; or redistribute material from this website, except for content specifically and
		expressly made available for redistribution (such as our newsletter). Where content
		is specifically made available for redistribution, it may only be redistributed within your
		business.<br>
		<br>

		<h3>LIMITATIONS OF WARRANTIES AND LIABILITY</h3>
		Whilst we endeavor to ensure that the information on this website is correct, we do
		not warrant its completeness or accuracy; nor do we commit to ensuring that the
		website remains available or that the material on the website is kept up-to-date.<br>
		<br>

		To the maximum extent permitted by applicable law we exclude all representations,
		warranties and conditions relating to this website and the use of this website
		(including, without limitation, any warranties implied by law of satisfactory quality,
		fitness for purpose and/or the use of reasonable care and skill).<br>
		<br>

		Nothing in this disclaimer (or elsewhere on our website) will exclude or limit our
		liability for fraud, for death or personal injury caused by our negligence, or for any
		other liability which cannot be excluded or limited under applicable law.<br>
		<br>

		Subject to this, our liability to you in relation to the use of our website or under or in
		connection with this disclaimer, whether in contract, tort (including negligence) or
		otherwise, will be limited as follows:

		<br>- to the extent that the website and the information and services on the website are provided free-of-charge, we will not be liable for any loss or damage of any nature;
		<br>- we will not be liable for any consequential, indirect or special loss or damage;
		<br>- we will not be liable for any loss of profit, income, revenue, anticipated savings,	contracts, business, goodwill, reputation, data, or information.
		<br><br>

		<h3>VARIATION</h3>
		We may revise these terms of use from time-to-time. Revised terms of use will apply
		to the use of our website from the date of the publication of the revised terms of use
		on our website. Please check this page regularly to ensure you are familiar with the
		current version.<br>
		<br>

		<h3>ENTIRE AGREEMENT</h3>
		This disclaimer, together with our PRIVACY POLICY constitutes the entire agreement
		between you and us in relation to your use of our website, and supersedes all
		previous agreements in respect of your use of this website.<br>
		<br>

		<h3>LAW AND JURISDICTION</h3>
		This disclaimer will be governed by and construed in accordance with Canadian
		law, and any disputes relating to this disclaimer will be subject to the non-exclusive
		jurisdiction of the courts of Toronto, Canada.<br>
		<br>

		<em>This document was last updated on: 15/03/2013</em>

	</p>


</div>
</section>
