<section>
<div class="page">

  <h1>Charting new territory for your brand.</h1>
  <h2>Brand Partnerships</h2>
  <p>
		Contribution: $30,000 <br>
		An opportunity for fresh thinking to push past creative boundaries and unlock potential ideas.
		Dozens of brilliant advertising campaigns that are immediately implementable. 
	</li>
	<p>Your brand owns all the creative. </p>
	<p>Our ‘brief’ experts will write the brief with your direction.</p>
	<p>Our strategic team and a member of the Jury will debrief you on the highlights of the Judges deliberations.</p>
	<p>Our implementation team is available to execute any of your submissions.</p>
	<ul>
		<li>Logo exposure to 8,000 Creatives and Media Planners </li>
		<li>Exclusive rights to your category </li>
		<li>Recognized as a leader promoting creativity in advertising </li>
		<li>Frequent posts to social media </li>
		<li>6 tickets to the Gala </li>
		<li>Promotional item in swag bag</li>
	</ul>
	<p>
		<a class="big-link" href="/model/2016/pdf/sponsorship_package-May-2016.pdf" target="_blank">
			Download the brochure</a> and find out how little you’ll have to pay to have hundreds of
			the country's top advertising agencies focus on your brand.
	</p>

</div>
</section>
