<section>
<div class="page">

	<h1>Yes, you can sit with us.</h1>

	<h2>2015 Sponsors</h2>
	<div class="sponsors sponsors2015">
	<?php // Sponsors
		$sponsors2015[] = array('name' => 'Pepsi', 'image' => 'pepsi');
		$sponsors2015[] = array('name' => 'Lays', 'image' => 'lays');
		$sponsors2015[] = array('name' => 'McDonald\'s', 'image' => 'mcd');
		$sponsors2015[] = array('name' => 'TD', 'image' => 'td');
		$sponsors2015[] = array('name' => 'spongetowels', 'image' => 'spongetowels');
		$sponsors2015[] = array('name' => 'cashmere', 'image' => 'cashmere');
		$sponsors2015[] = array('name' => 'BBDO', 'image' => 'bbdo');
		$sponsors2015[] = array('name' => 'kruger', 'image' => 'kruger');
		$sponsors2015[] = array('name' => 'cj', 'image' => 'cj');
		$sponsors2015[] = array('name' => 'sunnybrook', 'image' => 'sunnybrook');
		$sponsors2015[] = array('name' => 'RBC', 'image' => 'rbc');
		$sponsors2015[] = array('name' => 'kraft', 'image' => 'kraft');
		$sponsors2015[] = array('name' => 'scotties', 'image' => 'scotties');
		$sponsors2015[] = array('name' => 'yamaha', 'image' => 'yamaha');
		$sponsors2015[] = array('name' => 'nabob', 'image' => 'nabob');
		$sponsors2015[] = array('name' => 'zulu alpha kilo', 'image' => 'zulu');

		foreach ($sponsors2015 as $sponsor) {
			echo '<img alt="'.$sponsor['name'].'" src="/model/2015/sponsors_logos/original/'.$sponsor['image'].'.png" width="150"/>';
		}
	?>
	</div>

	<hr size="1"/>

	<h2>2014 Sponsors</h2>
	<div class="sponsors sponsors2014">
	<?php // Sponsors
		$sponsors2014[] = 'BBDO';
		$sponsors2014[] = 'BullsEye';
		$sponsors2014[] = 'Canada_WOF';
		$sponsors2014[] = 'CLight2';
		$sponsors2014[] = 'Elte_market';

		$sponsors2014[] = 'Envirocare';
		$sponsors2014[] = 'Hailo';
		$sponsors2014[] = 'Jello';
		$sponsors2014[] = 'Kraft';
		$sponsors2014[] = 'Kruger';

		$sponsors2014[] = 'OCAD';
		$sponsors2014[] = 'Pledge';
		$sponsors2014[] = 'RBC';
		$sponsors2014[] = 'SCJ';
		$sponsors2014[] = 'Somerset';

		$sponsors2014[] = 'TC_media';
		$sponsors2014[] = 'ZoomMedia';

		foreach ($sponsors2014 as $sponsor) {
			echo '<img alt="sponsors" src="/model/2014/sponsors_logos/'.$sponsor.'.jpg" width="150"/>';
		}
	?>
	</div>

	<hr size="1"/>

  <h2>2013 Sponsors</h2>
  <div class="sponsors sponsors2013">
	<?php // Sponsors
		$sponsors2013[] = 'aol';
		$sponsors2013[] = 'campbells';
		$sponsors2013[] = 'CV';
		$sponsors2013[] = 'draftfcb';
		$sponsors2013[] = 'fooddudes';

		$sponsors2013[] = 'frostcat';
		$sponsors2013[] = 'mediavest';
		$sponsors2013[] = 'ocad';
		$sponsors2013[] = 'oreo';
		$sponsors2013[] = 'pace';

		$sponsors2013[] = 'porsche';
		$sponsors2013[] = 'scj';
		$sponsors2013[] = 'springfree';
		$sponsors2013[] = 'subway';
		$sponsors2013[] = 'tennis';

		$sponsors2013[] = 'uber';
		$sponsors2013[] = 'yahoo';
		$sponsors2013[] = 'ziploc';

		foreach ($sponsors2013 as $sponsor) {
			echo '<img alt="sponsors" src="/model/2013/sponsors/'.$sponsor.'.png" width="150"/>';
		}
	?>
	</div>

</div>
</section>
