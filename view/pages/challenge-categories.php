<section>
	<div class="page">

		<h1>Mapping out your trip to cannes<h1>
		<h2>2016 Categories</h2>
		<p>
			2016 winners choose from the categories below, download the brief, win a trip to Cannes.
			It’s that simple. Well, except the winning part. That’s actually pretty tough.
		</p>
		<ul class="categories">
			<?php
				// ---- Category information
				include 'model/2016/category.php';
				foreach($categories as $key => $category){
			?>
				<li class="category">
					<!-- <img src="/model/2015/sponsors_logos/<?=$category['sponsor-logo'];?>" alt="<?=$category['title'];?>" width="150"> -->
					<h3><?=$category['title'];?></h3><br><br>
					<!-- <p><?=$category['text'];?></p> -->
				</li>
			<?php } ?>
		</ul>

	</div>
</section>
