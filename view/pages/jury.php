<section>
<div class="page">

	<h1>If Cannes is your destination, these people are your destiny.</h1>
  <h2>Jury</h2>

  <article>
		<div class="jury-chair">
			<h2 class="title">Lead Creative Co-Chairs</h2>

			<img src="/model/2015/jury/Ari Elkouby.jpg" alt="Ari Elkouby" width="210"><img src="/model/2015/jury/Nellie Kim.jpg" alt="Nellie Kim" width="210">

			<div class="jury-info">
				<div class="jury-info-first">
					<span class="jury-info-name">Ari Elkouby</span>
					<span class="jury-info-position">Creative Director,<br>Zulu Alpha Kilo Inc.</a></span>
				</div>
				<div class="jury-info-second">
					<span class="jury-info-name">Nellie Kim</span>
					<span class="jury-info-position">Partner, Co-Creative<br>Director, lg2</a></span>
				</div>
			</div>
		</div>

		<div class="jury-chair">
			<h2 class="title">Media Innovation Chair</h2>

			<img src="/model/2015/jury/Aaron Nemoy.png" alt="Aaron Nemoy" width="420">
			<div class="jury-info">
				<span class="jury-info-name">Aaron Nemoy</span>
				<span class="jury-info-position">Senior Brand Manager,<br>Kraft Foods</span>
			</div>
		</div>

	</article>
	<article>

		<div class="jury-member">
			<h2 class="title">Lead Creative Jury</h2>

			<ul>
				<?php
					// List of Jury
					$members[0] = array('img' => 'Natalie Armata.jpg', 'name' => 'Natalie Armata', 'position' => 'Co&nbsp;Founder and Creative&nbsp;Director', 'agency' => 'Giants and Gentleman');
					$members[1] = array('img' => 'Joseph Bonnici.jpg', 'name' => 'Joseph Bonnici', 'position' => 'Partner and Creative&nbsp;Director', 'agency' => 'Bensimon Byrne');
					$members[2] = array('img' => 'Pete Brenton.jpg', 'name' => 'Pete Breton', 'position' => 'Partner', 'agency' => 'Anomaly Toronto');
					$members[3] = array('img' => 'Brent Choi.jpg', 'name' => 'Brent Choi', 'position' => 'Chief&nbsp;Creative and Integration&nbsp;Officer', 'agency' => 'JWT Canada');
					$members[4] = array('img' => 'Gerald Kugler.jpg', 'name' => 'Gerald Kugler', 'position' => 'Creative&nbsp;Director', 'agency' => 'TBWA Toronto');
					$members[5] = array('img' => 'Dre Labre.jpg', 'name' => 'Dre Labre', 'position' => 'Partner and Creative&nbsp;Director', 'agency' => 'Rethink');
					$members[6] = array('img' => 'Lance Martin.jpg', 'name' => 'Lance Martin', 'position' => 'Executive Creative&nbsp;Director', 'agency' => 'Union');
					$members[7] = array('img' => 'Andrew Simon.jpg', 'name' => 'Andrew Simon', 'position' => 'Chief Creative&nbsp;Officer', 'agency' => 'Cundari');
					$members[8] = array('img' => 'Heather Hnatiuk.jpg', 'name' => 'Heather Hnatiuk', 'position' => 'Creative Director / Writer', 'agency' => 'Freelance');
					foreach ($members as $jury) {
				?>
					<li>
						<img src="/model/2015/jury/<?=$jury['img'];?>" alt="<?=$jury['name'];?>" width="183">
						<div class="jury-info">
							<span class="jury-info-name"><?=$jury['name'];?></span>
							<span class="jury-info-position"><?=$jury['position'];?><br><?=$jury['agency'];?></span>
						</div>
					</li>
				<?php } ?>
			</ul>
		</div>

		<div class="jury-member">
			<h2 class="title">Media Innovation Jury</h2>

			<ul>
				<?php
					// List of Jury
					unset($members);
					$members[0] = array('img' => 'Suzanne Ware.jpg', 'name' => 'Suzanne Ware', 'position' => 'Director&nbsp;Media', 'agency' => 'Kraft Foods');
					$members[1] = array('img' => 'Sean McDonald.jpg', 'name' => 'Sean McDonald', 'position' => 'Vice&nbsp;President, Brand&nbsp;Experience', 'agency' => 'Taxi Canada');
					$members[3] = array('img' => 'Steve Meraska.jpg', 'name' => 'Steve Meraska', 'position' => 'SVP&nbsp;Innovation', 'agency' => 'Starcom MediaVest');
					$members[4] = array('img' => 'Matt Ramella.jpg', 'name' => 'Matt Ramella', 'position' => 'VP&nbsp;Digital', 'agency' => 'UM');
					$members[5] = array('img' => 'Richard Fofana.jpg', 'name' => 'Richard Fofana', 'position' => 'VP&nbsp;Strategy', 'agency' => 'UM');
					$members[6] = array('img' => 'Craig Jennings.jpg', 'name' => 'Craig Jennings', 'position' => 'Director&nbsp;Media / Agency&nbsp;Management', 'agency' => 'RBC');
					foreach ($members as $jury) {
				?>
					<li>
						<img src="/model/2015/jury/<?=$jury['img'];?>" alt="<?=$jury['name'];?>" width="226">
						<div class="jury-info">
							<span class="jury-info-name"><?=$jury['name'];?></span>
							<span class="jury-info-position"><?=$jury['position'];?><br><?=$jury['agency'];?></span>
						</div>
					</li>
				<?php } ?>
			</ul>
		</div>

	</article>

</div>
</section>
