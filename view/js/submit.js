// this identifies your website in the createToken call below
Stripe.setPublishableKey('pk_live_inn7CDHAJkMHh4M4eiXlrhhV');
//Stripe.setPublishableKey('pk_test_1EaLLhKJJTfUUC7lNKS9KukG');

function stripeResponseHandler(status, response) {

	if (response.error)
	{
		console.log(response.error);
		$('span.card-fail').empty();

		// Show the error messages about card info
		if(response.error.code === "invalid_number" || response.error.code === "incorrect_number")
		{
			$('span.card-number').text("provided invalid");
		}
		else if(response.error.code === "invalid_expiry_year" || response.error.param === "exp_year")
		{
			$('span.card-expiry').text("Year invalid");
		}
		else if(response.error.code === "invalid_expiry_month" || response.error.param === "exp_month")
		{
			$('span.card-expiry').text("Month invalid");
		}
		else if(response.error.code === "invalid_cvc")
		{
			$('span.card-cvc').text("is invalid");
		}
		else if(response.error.code === "card_declined")
		{
			$('span.payment-errors').text('The card was declined');
		}

		$('.submit-button').prop('disabled', false);
	}
	else
	{
		var $form = $('#payment-form');
		// token contains id, last4, and card type
		var token = response.id;
		// Insert the token into the form so it gets submitted to the server
		$form.append($('<input type="hidden" name="stripeToken" />').val(token));
		// and submit
		$form.get(0).submit();
	}
}

$('#payment-form').submit(function(event) {

	// Disable the submit button to prevent repeated clicks
	$('.submit-button').prop('disabled', true);

	Stripe.createToken({
		number: $('input.card-number').val(),
		cvc: $('input.card-cvc').val(),
		exp_month: $('input.card-expiry-month').val(),
		exp_year: $('input.card-expiry-year').val(),
		name: $('input.card-name').val()
	}, stripeResponseHandler);

	// Prevent the form from submitting with the default action
	return false;
});
