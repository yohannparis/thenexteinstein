// ---- Login

	// Test if the button exist
	var dashboardLoginBtn = document.getElementsByClassName('dashboard-login-btn')[0];
	if (typeof(dashboardLoginBtn) != 'undefined' && dashboardLoginBtn != null){

		// Variables
		var dashboardLogin = document.getElementsByClassName('dashboard-login')[0];
		var dashboardEmail = document.getElementById('dashboardEmail');
		var dashboardPassword = document.getElementById('dashboardPassword');
		var dashboardButton = document.getElementById('dashboardButton');
		var dashboardUrl = '/dashboard/login';

		// Open the login pop-up
		dashboardLoginBtn.addEventListener('click', function(e){
			e.preventDefault;

			// Check if the dashboard login pop-up is aleady open
			if (dashboardLogin.className.search('active') != -1){
				dashboardLogin.className = 'dashboard-login';
			} else {
				dashboardLogin.className += " active";
				dashboardEmail.focus();
			}
		});

		// Dashboard login handler
		function dashboardHandler(button){

			var dashboardError = document.getElementById('dashboardError');

			try{
				if(httpRequest.readyState < 4){
					httpRequest.button.classList.add('ajax-spinner');

				} else if(httpRequest.readyState === 4){

					if(httpRequest.status === 200){
						var response = JSON.parse(httpRequest.responseText);
						console.log(response);

						if(response.sent){
							httpRequest.button.classList.remove('ajax-spinner');
							httpRequest.button.classList.add('ajax-success');
							location = "/dashboard";

						} else {
							httpRequest.button.classList.remove('ajax-spinner');

							dashboardError.classList.add('display');
							if (response.message == 'ERROR_INFORMATION'){
								dashboardError.innerHTML = 'You do have to fill this stuff out, you know. <a href="/create-account">Create an account?</a>';
							} else if (response.message == 'ERROR_LOG_IN'){
								dashboardError.innerHTML = 'Account using this email already exist. <a href="/forget-password">Forget your password?</a>';
							} else if (response.message == 'ERROR_DO_NOT_EXIST'){
								dashboardError.innerHTML = 'Sorry, this email is not registered. <a href="/create-account">Create an account?</a>';
							} else {
								dashboardError.innerHTML = 'Email and/or Password are not valid. <a href="/create-account">Create an account?</a>';
							}
						}

					} else {
						dashboardError.classList.add('display');
						dashboardError.innerHTML = 'Sorry, something went wrong, please try later.';
						console.log('Error wrong response.');
					}
				}
			} catch(e){
				dashboardError.classList.add('display');
				dashboardError.innerHTML = 'Sorry, something went wrong, please try later.';
				console.log('Error Server down.');
			}
		}

		// Send the email and password to login into the dashboard
		dashboardButton.addEventListener('click', function (event){
			event.preventDefault();
			var dashboardData = 'email='+dashboardEmail.value+'&password='+encodeURIComponent(dashboardPassword.value);
			makeRequest(dashboardButton, dashboardHandler, dashboardUrl, dashboardData);
		});

	}
