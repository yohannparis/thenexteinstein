// Ajax functions
var httpRequest;

function makeRequest(button, handler, url, data){
	if (window.XMLHttpRequest) { // Mozilla, Safari, ...
		httpRequest = new XMLHttpRequest();
	} else if (window.ActiveXObject) { // IE
		try { httpRequest = new ActiveXObject("Msxml2.XMLHTTP"); }
		catch (e) {
			try { httpRequest = new ActiveXObject("Microsoft.XMLHTTP"); }
			catch (e) {
				console.log('Error no IE AJAX API.');
			}
		}
	}

	if(!httpRequest){
		console.log('Error could not make an AJAX object.')
		return false;
	}

	httpRequest.button = button;
	httpRequest.onreadystatechange = handler;
	httpRequest.open('POST', url);
	httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	httpRequest.send(data);
}
