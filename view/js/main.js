// ---- Navigation

	window.onload = function() {
		window.onscroll = function (){

			// Change the Main navigation from top to sticky
			if (window.pageYOffset < 130){
				document.body.classList.remove('sticky-main-nav');
				document.body.classList.add('top-main-nav');
			} else {
				document.body.classList.remove('top-main-nav');
				document.body.classList.add('sticky-main-nav');
			}

			// Change the winners navigation from normal to fixed
			var winnersNav = document.getElementsByClassName('winners-nav')[0];
			if (typeof(winnersNav) != 'undefined' && winnersNav != null){
				if (window.pageYOffset > 350){
					winnersNav.className = 'winners-nav fixed';
				} else {
					winnersNav.className = 'winners-nav float';
				}
			}
		}
	}

// ---- Menu code for Mobile only

	// ...Didn't wanted to had server detection for less than 200 bytes
	var mainNav = document.getElementsByClassName('main-nav')[0];
	mainNav.addEventListener('click', function(event){
		// Only open and close if we click on the main-nav button (:after) and not on the links
		if(event.srcElement.classList.contains('main-nav')){
			document.body.classList.toggle('nav-open');
		}
	});

// ---- Submit STEP 1

	// -- Some basic JS to remove all the disable on Entrant 2 inputs if the user wants to add one.
	function changeDisabled(E2){

		var E2s = document.querySelectorAll('[name^="E2_"]');

		// The Entrant 2 is checked
		if(E2.checked){
			// Removing the disabled input
			for (var i = E2s.length - 1; i >= 0; i--) {
				E2s[i].removeAttribute('disabled');
			};

		// The Entrant 2 info isn't choosen
		} else {
			// Add the disbled to the input
			for (var i = E2s.length - 1; i >= 0; i--) {
				E2s[i].setAttribute('disabled', 'disabled');
			};
		}
	}

	// Add the test on page load and on click fron the user
	var E2 = document.querySelector('input[name="E2"]');
	// Check it exist
	if (typeof(E2) != 'undefined' && E2 != null){
		changeDisabled(E2);
		E2.addEventListener('change', function() {
			changeDisabled(E2);
		});
	}
