// ---- Analytics

	// Google Analytics
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-58597401-1', 'auto');
	ga('send', 'pageview');

	//Analytics for NAC from outsource company
	var _ss = _ss || [];
	_ss.push(['_setDomain', 'https://koi-16DGM2G.sharpspring.com/net']);
	_ss.push(['_setAccount', 'KOI-1F5GJ7I']);
	_ss.push(['_trackPageView']);
	(function() {
		var ss = document.createElement('script');
		ss.type = 'text/javascript'; ss.async = true;
		ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-16DGM2G.sharpspring.com/client/ss.js?ver=1.1.1';
		var scr = document.getElementsByTagName('script')[0];
		scr.parentNode.insertBefore(ss, scr);
	})();

