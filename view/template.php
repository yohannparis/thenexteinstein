<?php
	global $logged;
	header('content-type:text/html; charset=utf-8');
  if (!isset($section)){ $section = null; }
  if (!isset($seo_description)){ $seo_description = SEODESC; }
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="initial-scale=1">

	<title>2015 - NAC - <?=$description;?></title>
	<meta name="description" content="<?=$seo_description;?>">

	<meta property="og:type" content="website">
	<meta property="og:url" content="http://thenexteinstein.com/">
	<meta property="og:image" content="http://thenexteinstein.com/view/images/TNE_Logo.png">
	<meta property="og:title" content="2015 - NAC - <?=$description;?>">
	<meta property="og:description" content="<?=$seo_description;?>">

	<link href='http://fonts.googleapis.com/css?family=EB+Garamond|Open+Sans:700,400' rel='stylesheet' type='text/css'>
	<link href="/view/css/shame.css?v=2015-06-14" rel="stylesheet" type="text/css">

	<link rel="shortcut icon" href="/view/images/TNE_Logo.png">
	<link rel="apple-touch-icon" href="/view/images/TNE_Logo.png">

</head>
<body class="top-main-nav" id="<?=$section;?>">
<header>
	<ul class="main-nav">
		<li><a class="home-link" href="/"><img src="/view/images/TNE_Logo.png" alt="the next einstein" border="0" /></a></li>

		<li class="item"><a class="<?php if ($section == 'home'){ echo "active"; } ?>" href="/">Home</a></li>

		<?php /*
		<li class="item sub-menu">
			<a class="<?php if ($section == 'challenge'){ echo "active"; } ?>" href="/about-the-challenge">The&nbsp;Challenge</a>

			<ul class="challenge">
				<li><a href="/about-the-challenge">About&nbsp;the&nbsp;challenge</a></li>
				<li><a href="/categories">Categories</a></li>
				<li><a href="/prizing">Prizing</a></li>
			</ul>
		</li>
		*/ ?>

		<li class="item"><a class="<?php if ($section == 'winners'){ echo "active"; } ?>" href="/winners">Winners</a></li>

		<?php /*
		<li class="item"><a class="<?php if ($section == 'jury'){ echo "active"; } ?>" href="/jury">Jury</a></li>

		<li class="item sub-menu">
			<a class="<?php if ($section == 'sponsorship'){ echo "active"; } ?>" href="/sponsorships">Sponsorships</a>
			<ul class="sponsorships">
				<li><a href="/sponsorships">Overview&nbsp;sponsorship</a></li>
				<li><a href="/media-sponsorships">Media&nbsp;sponsorship</a></li>
				<li><a href="/brand-sponsorships">Brand&nbsp;sponsorship</a></li>
				<li><a href="/presenting-sponsorships">Presenting&nbsp;sponsorship</a></li>
				<li><a href="/list-sponsorships">List&nbsp;of&nbsp;sponsors</a></li>
			</ul>
		</li>

		<li class="item">
			<?php if ($logged and $section == 'dashboard'){ ?>
				<a href="/logout">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Log out &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
			<?php } else if ($logged){ ?>
				<a href="/dashboard">&nbsp;Dashboard&nbsp;</a>
			<?php } else{ ?>
				<a class="dashboard-login-btn" href="javascript:void(0)">Log In/Enter</a>
				<form class="dashboard-login" method="post" action="">
					<a href="/create-account">Create an account</a>
					<input id="dashboardEmail" type="email" name="email" placeholder="EMAIL">
					<input id="dashboardPassword" type="text" name="password" placeholder="PASSWORD">
					<a href="/forget-password">Forget your Password?</a>
					<span id="dashboardError" class="msg-fail"></span>
					<input id="dashboardButton" type="submit" name="submit" value="Sign In">
				</form>
			<?php } ?>
		</li>
		*/ ?>
	</ul>
</header>

	<?php
		if($section == 'submit'){
			include 'view/submit/'.$page.'.php';
		} else {
			include 'view/pages/'.$page.'.php';
		}
	?>

<?php /*
<div class="sponsors-list">
	<div>
	<?php // Sponsors
		$sponsors[] = array('name' => 'Pepsi', 'image' => 'pepsi');
		$sponsors[] = array('name' => 'Lays', 'image' => 'lays');
		$sponsors[] = array('name' => 'McDonalds', 'image' => 'mcd');
		$sponsors[] = array('name' => 'spongetowels', 'image' => 'spongetowels');
		$sponsors[] = array('name' => 'cashmere', 'image' => 'cashmere');
		$sponsors[] = array('name' => 'scotties', 'image' => 'scotties');
		$sponsors[] = array('name' => 'kruger', 'image' => 'kruger');
		$sponsors[] = array('name' => 'RBC', 'image' => 'rbc');

		$sponsors[] = array('name' => 'TD', 'image' => 'td');
		$sponsors[] = array('name' => 'nabob', 'image' => 'nabob');
		$sponsors[] = array('name' => 'kraft', 'image' => 'kraft');
		$sponsors[] = array('name' => 'sunnybrook', 'image' => 'sunnybrook');
		$sponsors[] = array('name' => 'yamaha', 'image' => 'yamaha');
		$sponsors[] = array('name' => 'BBDO', 'image' => 'bbdo');
		$sponsors[] = array('name' => 'zulu alpha kilo', 'image' => 'zulu');
		$sponsors[] = array('name' => 'cj', 'image' => 'cj');

		foreach ($sponsors as $sponsor) {
			echo '<img alt="'.$sponsor['name'].'" src="/model/2015/sponsors_logos/'.$sponsor['image'].'.png" width="115" height="115"/>';
		}
	?>
	</div>
</div>
*/ ?>

<footer>
	<div>
		<?php /*
		<nav>
			<h4>What is TNE?</h4>
			<a href="/about-the-challenge">About the Competition</a>
			<a href="/categories">Categories</a>
			<a href="/prizing">Prizing</a>
		</nav>

		<nav>
			<h4>JOIN NOW</h4>
			<a href="/entry-information">Entry Information</a>
			<a href="/entry-specifications">Entry Specifications</a>
			<a href="/sponsorships">Sponsorship</a>
			<a href="/briefs">Briefs</a>
		</nav>

		<nav>
			<h4>FINE PRINT</h4>
		  <a href="/rules-and-regulations">Rules &amp; Regulations</a>
		  <a href="/terms-and-conditions">Terms &amp; Conditions</a>
		  <a href="/privacy-policy">Privacy Policy</a>
		  <a href="/legal">Legal</a>
		</nav>
		*/ ?>

		<nav>
			<h4>CONTACT</h4>
		  <a href="https://goo.gl/maps/AT2eS">
		  	<strong>Canadian Friends of The Hebrew<br>University of Jerusalem</strong><br>
		  	3080 Yonge Street, Suite 3020<br>
		  	PO Box 65, Toronto, ON M4N 3N1
		  </a>
		  <a href="tel:+14164858000">(416)485-8000</a>
		</nav>

		<nav>
			<h4>FOLLOW US</h4>
			<a class="social-media" href="https://www.facebook.com/nationaladvertisingchallenge">
				<img src="/view/images/social/facebook.gif" width="31" height="32"></a>
			<a class="social-media" href="https://twitter.com/HelloNAC">
				<img src="/view/images/social/twitter.gif" width="31" height="32"></a>

		  <h4>SUBSCRIBE TO NEWSLETTER</h4>
			<input id="newsletterEmail" type="text" name="email">
			<button id="newsletterSubscribe">Subscribe</button>
		</nav>

	</div>

	<div>
		<div>
			<p>
				<a href="/contact">Contact</a>
			</p>
			<img src="/view/images/CFHU.png" alt="CFHU" border="0" width="175px"></a>
			<p>
				The Next Einstein initiative was established in 2013 by the Canadian Friends of the Hebrew
				University of Jerusalem in conjunction with The Hebrew University of Jerusalem: the custodian
				of the Einstein estate. Albert Einstein was one of the founding fathers of the&nbsp;university.
			</p>
		</div>
	</div>
</footer>

<!--[if lt IE 9]><script async src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script><![endif]-->
<!--[if lt IE 9]><script async src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script async src="//cdn.polyfill.io/v1/polyfill.js"></script>
<script async src="/view/js/prod.js"></script>

</body>
</html>
