<?php

// Test the URL to know where to redirect the user
if (
	preg_match('/thenexteinstein.dev.10.0.0.127.xip.io/', $_SERVER["SERVER_NAME"]) == false
	and
	preg_match('/thenexteinstein.dev/', $_SERVER["SERVER_NAME"]) == false
	and
	preg_match('/localhost:5757/', $_SERVER["SERVER_NAME"]) == false
	and
	preg_match('/10.0.0.127:5757/', $_SERVER["SERVER_NAME"]) == false
){
	header('Location: https://thenexteinstein.com/');
}

// ---- Setup the URL of the website

// Dev
define('URL','http://thenexteinstein.dev.10.0.0.127.xip.io/');

// Prod
//define('URL','https://thenexteinstein.com/');
