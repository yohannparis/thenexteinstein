<?php

/*
 * Global Constants
 */

// ---- display no errors online
//error_reporting(0);
error_reporting(E_ERROR | E_WARNING | E_PARSE);

// ---- Setup the URL of the website
require_once 'server.php';

// ---- Set include path
$path = '/Users/admin/Development/thenexteinstein.com/';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

// ---- Date
date_default_timezone_set('America/Toronto');
$date = new dateTime('now');
define('DATE', $date->format('Y-m-d H:i'));

// ---- SEO info
define('SEODESC','The Next Einstein is a competition with an audacious goal: to discover and celebrate ideas that could change the world.');

// ---- Setup prices of entry
$priceFull = 175;
$priceDiscount = 50;
$priceStudent = 0;

// ---- Setup prices of gala ticketx
$priceTicket = 60;
$maxTickets = 300;

/*
 * Sessions and Router
 */

ob_start('ob_gzhandler'); // Compress output and turn on buffer

require_once 'controller/functions.php';
require_once 'controller/database.php';
require_once 'controller/stats.php';
require_once 'controller/email.php';
require_once 'controller/users.php';
require_once 'controller/sessions.php';
require_once 'controller/entries.php';
require_once 'controller/route.php';

ob_end_flush(); // Send the Output Buffering
